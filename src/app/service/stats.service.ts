import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Constants } from '../constants/constant';
import { handleErrorObservable } from '../utilities/helper';
import getDateInSqlFormat from '../utilities/sql-date-convertor';

@Injectable({
    providedIn: 'root',
})
export class StatsService {
    constructor(private httpClient: HttpClient) { }

    getNewsletterCount(): Observable<any> {
        let params: HttpParams = new HttpParams();
        params = params.append('$limit', "0");
        return this.httpClient.get(`${Constants.BASEURL}/newsletter`, { params }).pipe(
            map(res => res['total']),
            catchError(handleErrorObservable<any>('error in getNewsletterCount '))
        );
    }

    getOverdueTasksCount(): Observable<any> {
        let params: HttpParams = new HttpParams();
        params = params.append('$limit', "0");
        params = params.append('due_date[$lt]', getDateInSqlFormat(new Date()));
        return this.httpClient.get(`${Constants.BASEURL}/task`, { params }).pipe(
            map(res => res['total']),
            catchError(handleErrorObservable<any>('error in getOverdueTasksCount '))
        );
    }

    getClosedNewsItemsCount(): Observable<any> {
        let params: HttpParams = new HttpParams();
        params = params.append('$limit', "0");
        params = params.append('workflow_state_id', "6");
        return this.httpClient.get(`${Constants.BASEURL}/newsitem`, { params }).pipe(
            map(res => res['total']),
            catchError(handleErrorObservable<any>('error in getNewsletterCount '))
        );
    }

    getOpenNewsItemsCount(): Observable<any> {
        let params: HttpParams = new HttpParams();
        params = params.append('$limit', "0");
        params = params.append('workflow_state_id[$ne]', "6");
        return this.httpClient.get(`${Constants.BASEURL}/newsitem`, { params }).pipe(
            map(res => res['total']),
            catchError(handleErrorObservable<any>('error in getNewsletterCount '))
        );
    }

    getRelevantItemsCount(): Observable<any> {
        let params: HttpParams = new HttpParams();
        params = params.append('$limit', "0");
        params = params.append('status_id', "1");
        return this.httpClient.get(`${Constants.BASEURL}/newsitem`, { params }).pipe(
            map(res => res['total']),
            catchError(handleErrorObservable<any>('error in getNewsletterCount '))
        );
    }

    getNotRelevantItemsCount(): Observable<any> {
        let params: HttpParams = new HttpParams();
        params = params.append('$limit', "0");
        params = params.append('status_id', "2");
        return this.httpClient.get(`${Constants.BASEURL}/newsitem`, { params }).pipe(
            map(res => res['total']),
            catchError(handleErrorObservable<any>('error in getNewsletterCount '))
        );
    }

    getClassificationCount(id: string): Observable<any> {
        let params: HttpParams = new HttpParams();
        params = params.append('$limit', "0");
        params = params.append('classification_id', id);
        return this.httpClient.get(`${Constants.BASEURL}/newsitem-class`, { params }).pipe(
            map(res => res['total']),
            catchError(handleErrorObservable<any>('error in getNewsletterCount '))
        );
    }

    getGraphData(classificationId: string, filter: string) {
        let params: HttpParams = new HttpParams();
        params = params.append('classification', classificationId);
        params = params.append('filter', filter);
        return this.httpClient.get(`${Constants.BASEURL}/stats`, { params }).pipe(
            catchError(handleErrorObservable<any>('error in getNewsletterCount ')));
    }
}
