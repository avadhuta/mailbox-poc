import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Constants } from '../constants/constant';
import { handleErrorObservable } from '../utilities/helper';
import { SortField } from '../entities/sort-field';
import { Classification } from '../entities/classification';
import { NewsItem } from '../entities/newsitem';
import { TaskStatus } from '../entities/task-status.enum';

@Injectable({
  providedIn: 'root',
})
export class NewsletterService {
  constructor(private httpClient: HttpClient) { }

  getAllNewsletters(
    limit: number,
    page: number,
    sortByField?: SortField[]
  ): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('$limit', '' + limit);
    params = params.append('$skip', '' + page * limit);
    if (sortByField && sortByField.length > 0) {
      sortByField.map(
        (field) =>
          (params = params.append(
            '$sort[' + field.name + ']',
            field.sortOrder === 'asc' ? '1' : '-1'
          ))
      );
    }
    return this.httpClient
      .get(`${Constants.BASEURL}/newsletter`, { params })
      .pipe(
        map((res: any) => {
          return res.data;
        }),
        catchError(handleErrorObservable<any>('error in getting newsletters'))
      );
  }

  getNewsletterById(id: string): Observable<any> {
    return this.httpClient
      .get(`${Constants.BASEURL}/newsletter/${id}`)
      .pipe(
        catchError(handleErrorObservable<any>('error in getting newsletters'))
      );
  }

  getNewsItemById(id: string): Observable<NewsItem> {
    return this.httpClient
      .get<NewsItem>(`${Constants.BASEURL}/newsitem/${id}`)
      .pipe(
        catchError(handleErrorObservable<any>('error in getting news item'))
      );
  }

  getNewsletterByMailId(mailId: string): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('mail_id', mailId);
    return this.httpClient
      .get(`${Constants.BASEURL}/newsletter`, { params })
      .pipe(
        map((res: any) => {
          return res.data.length > 0 ? res.data[0] : null;
        }),
        catchError(
          handleErrorObservable<any>('error in getting newsletter of mail id')
        )
      );
  }

  getNewsItemsOfNewsletter(
    newsLetterId: string,
    statusId: string,
    limit: number,
    page: number,
    sortByField?: SortField[],
    classification: string = null
  ): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('newsletter_id', newsLetterId);
    params = params.append('status_id', statusId);
    params = params.append('$limit', '' + limit);
    params = params.append('$skip', '' + page * limit);
    if (sortByField && sortByField.length > 0) {
      sortByField.map(
        (field) =>
          (params = params.append(
            '$sort[' + field.name + ']',
            field.sortOrder === 'asc' ? '1' : '-1'
          ))
      );
    }
    console.log(
      'NewsletterService -> constructor -> classification',
      classification
    );
    if (classification) {
      params = params.append('classification', classification);
      return this.httpClient
        .get(`${Constants.BASEURL}/query-newsitems`, { params })
        .pipe(
          map((res: any) => {
            console.log(
              'getNewsItemsOfNewsletter -> with classification',
              statusId,
              res,
              newsLetterId
            );
            return res;
          }),
          catchError(
            handleErrorObservable<any>(
              'error in getting news items of newsletter'
            )
          )
        );
    } else {
      return this.httpClient
        .get(`${Constants.BASEURL}/newsitem`, { params })
        .pipe(
          map((res: any) => {
            console.log(
              'getNewsItemsOfNewsletter -> no classification',
              statusId,
              res,
              newsLetterId
            );
            return res;
          }),
          catchError(
            handleErrorObservable<any>(
              'error in getting news items of newsletter'
            )
          )
        );
    }
  }

  getAllNewsItems(
    limit: number,
    page: number,
    statusId?: string,
    sortByField?: SortField[],
    classification: string = null
  ): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('$limit', '' + limit);
    params = params.append('$skip', '' + page * limit);
    if (statusId) {
      params = params.append('status_id', statusId);
    }
    if (sortByField && sortByField.length > 0) {
      sortByField.map(
        (field) =>
          (params = params.append(
            '$sort[' + field.name + ']',
            field.sortOrder === 'asc' ? '1' : '-1'
          ))
      );
    }

    if (classification) {
      params = params.append('classification', classification);
      return this.httpClient
        .get(`${Constants.BASEURL}/query-newsitems`, { params })
        .pipe(
          catchError(
            handleErrorObservable<any>(
              'error in getting news items of newsletter'
            )
          )
        );
    } else {
      return this.httpClient
        .get(`${Constants.BASEURL}/newsitem`, { params })
        .pipe(
          catchError(
            handleErrorObservable<any>('error in getAllInbox news items')
          )
        );
    }
  }

  getNewsItemsForGraphCondition(
    country: string,
    processId: string,
    receivedDate: Date,
    limit: number,
    page: number
  ): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('country', country);
    params = params.append('process_id', processId);
    params = params.append('received_date', '' + receivedDate.getTime());
    params = params.append('$limit', '' + limit);
    params = params.append('$skip', '' + page * limit);

    return this.httpClient
      .get(`${Constants.BASEURL}/query-newsitems-graph`, { params })
      .pipe(
        map((res: any) => {
          console.log('getNewsItemsForGraphCondition -> res', res);
          return res;
        }),
        catchError(
          handleErrorObservable<any>(
            'error in getting news items of getNewsItemsForGraphCondition'
          )
        )
      );
  }

  changeNewsItemStatus(newsItemId: string, statusId: string) {
    console.log("NewsletterService -> changeNewsItemStatus -> newsItemId", newsItemId);
    return this.httpClient.patch(
      `${Constants.BASEURL}/newsitem/${newsItemId}`,
      { status_id: statusId }
    );
  }

  changeNewsItemClassification(
    newsItemId: string,
    classificationIds: string[]
  ) {
    const params = classificationIds.map((id) => {
      return { newsitem_id: newsItemId, classification_id: id };
    });
    return this.httpClient.post(`${Constants.BASEURL}/newsitem-class`, params);
  }

  changeNewsItemWorkflowState(newsItemId: string, workflowStateId: number) {
    return this.httpClient.patch(
      `${Constants.BASEURL}/newsitem/${newsItemId}`,
      { workflow_state_id: workflowStateId }
    );
  }

  changeNewsItemCountryImpacted(newsItemId: string, country: string) {
    return this.httpClient.patch(
      `${Constants.BASEURL}/newsitem/${newsItemId}`,
      { country_impacted: country }
    );
  }
  changeNewsItemRegionImpacted(newsItemId: string, region: string) {
    return this.httpClient.patch(
      `${Constants.BASEURL}/newsitem/${newsItemId}`,
      { region_impacted: region }
    );
  }

  createNewsitemProcess(newsItemId: string, processes: any[]) {
    const params = processes.map((id) => {
      return { newsitem_id: newsItemId, process_id: id };
    });
    return this.httpClient.post(
      `${Constants.BASEURL}/newsitem-process`,
      params
    );
  }

  getNewsitemProcess(newsItemId: string) {
    let params: HttpParams = new HttpParams();
    params = params.append('newsitem_id', newsItemId);
    return this.httpClient
      .get(`${Constants.BASEURL}/newsitem-process`, {
        params,
      })
      .pipe(
        map((res: any) => {
          return res.data;
        })
      );
  }

  getWorkflowStates(): Observable<any> {
    return this.httpClient.get(`${Constants.BASEURL}/workflow-state`).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(handleErrorObservable<any>('error in getting workflow states'))
    );
  }

  getNewsItemStatuses() {
    return this.httpClient.get(`${Constants.BASEURL}/newsitem-status`).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(
        handleErrorObservable<any>('error in getting news items statuses')
      )
    );
  }

  getNewsItemClassifications(): Observable<Classification[]> {
    return this.httpClient
      .get<Classification[]>(`${Constants.BASEURL}/newsitem-classification`)
      .pipe(
        map((res: any) => {
          return res.data;
        }),
        catchError(
          handleErrorObservable<any>(
            'error in getting news items classification'
          )
        )
      );
  }

  getClassificationOfNewsItemById(
    newsitemId: string
  ): Observable<Classification[]> {
    let params: HttpParams = new HttpParams();
    params = params.append('newsitem_id', newsitemId);
    return this.httpClient
      .get<Classification[]>(`${Constants.BASEURL}/newsitem-class`, { params })
      .pipe(
        map((res: any) => {
          return res.data;
        }),
        catchError(
          handleErrorObservable<any>(
            'error in getting news items classification'
          )
        )
      );
  }

  getTasksOfNewsItem(newsitemId: string) {
    let params: HttpParams = new HttpParams();
    params = params.append('newsitem_id', newsitemId);
    return this.httpClient.get(`${Constants.BASEURL}/task`, { params }).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(handleErrorObservable<any>('error in getTasksOfNewsItem'))
    );
  }

  getTasksOfUser(userId: string) {
    let params: HttpParams = new HttpParams();
    params = params.append('user_assigned', userId);
    return this.httpClient.get(`${Constants.BASEURL}/task`, { params }).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(handleErrorObservable<any>('error in getTasksOfAssessment'))
    );
  }

  completeTask(taskId: string) {
    return this.httpClient
      .patch(`${Constants.BASEURL}/task/${taskId}`, {
        status_id: TaskStatus.Completed,
      })
      .pipe(catchError(handleErrorObservable<any>('error in complete task')));
  }
}
