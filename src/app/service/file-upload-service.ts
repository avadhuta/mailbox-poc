import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Constants } from '../constants/constant';
import { handleErrorObservable } from '../utilities/helper';

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
  constructor(private http: HttpClient) {}

  uploadFileToS3(url: string, file, fileExt): Observable<any> {
    const headers = this.setHeaderBasedOnMimeType(fileExt);
    console.log('setting headers for S3 upload ', headers, file, fileExt);
    return this.http.put(url, file, { headers });
  }

  getUploadUrl(type: string): Observable<any> {
    const url = Constants.BASEURL + '/document';
    const params = { filetype: type };
    console.log(params, 'params');
    return this.http.post(url, params).pipe(
      map((res) => {
        const mapedurl = res['url'];
        return { mapedurl };
      })
    );
  }

  setHeaderBasedOnMimeType(fileExt) {
    console.log('fetching headers for ', fileExt);
    let contentType = '';
    if (fileExt === 'jpeg') {
      contentType = 'image/' + fileExt;
    } else if (fileExt === 'doc' || fileExt === 'dot' || fileExt === 'docx') {
      contentType = 'application/msword/' + fileExt;
    } else if (fileExt === 'txt') {
      contentType = 'text/' + fileExt;
    } else if (fileExt === 'xlsx') {
      contentType =
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet/' +
        fileExt;
    } else if (fileExt === 'xls' || fileExt === 'xlt' || fileExt === 'xla') {
      contentType = 'application/vnd.ms-excel/' + fileExt;
    } else if (fileExt === 'pdf') {
      contentType = 'application/' + fileExt;
    } else if (fileExt === 'csv') {
      contentType = 'text/' + fileExt;
    }
    console.log('content type for header: ', contentType);
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', contentType);
    return headers;
  }

  downloadFile(url: string): Observable<Blob> {
    return this.http.get(url, { responseType: 'blob' });
  }
}
