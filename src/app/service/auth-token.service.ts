import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class AuthTokenService {
  readonly STORAGE_KEY_AUTH = 'mail-box-auth-token';
  private authToken: string;

  constructor(private localStorage: LocalStorageService) {}

  setToken(token) {
    this.authToken = token;
    this.saveToken(token);
  }

  getToken() {
    return this.getSavedToken();
  }

  clear() {
    this.localStorage.remove(this.STORAGE_KEY_AUTH);
    this.authToken = null;
  }

  private saveToken(token) {
    // console.log('saving token : ' + token);
    this.localStorage.set(this.STORAGE_KEY_AUTH, token);
  }

  private getSavedToken(): string {
    // get from local storage
    return this.localStorage.get<string>(this.STORAGE_KEY_AUTH);
  }
}
