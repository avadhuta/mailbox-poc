import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Constants } from '../constants/constant';
import { handleErrorObservable } from '../utilities/helper';
import { Assessment } from '../entities/assessment';
import { Action } from '../entities/action';
import { Requirement } from '../entities/requirement';
import { Task } from '../entities/task';

@Injectable({
  providedIn: 'root',
})
export class AssessmentService {
  constructor(private httpClient: HttpClient) {}

  getAssessmentOfNewsItem(newsItemId: string): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('newsitem_id', newsItemId);
    return this.httpClient
      .get(`${Constants.BASEURL}/assessment`, { params })
      .pipe(
        map((res: any) => {
          if (res.data && res.data.length > 0) return res.data[0];
          else return null;
        }),
        catchError(
          handleErrorObservable<any>('error in getAssessmentsOfNewsItem')
        )
      );
  }

  createAssessment(assessment: Assessment) {
    return this.httpClient.post(`${Constants.BASEURL}/assessment`, assessment);
  }

  createRequirement(requirement: Requirement) {
    return this.httpClient.post(
      `${Constants.BASEURL}/requirement`,
      requirement
    );
  }

  createAction(action: Action) {
    return this.httpClient.post(`${Constants.BASEURL}/action`, action);
  }

  createTask(task: Task) {
    return this.httpClient.post(`${Constants.BASEURL}/task`, task);
  }

  getTasksOfNewsItem(newsItemId: string): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('newsitem_id', newsItemId);
    return this.httpClient.get(`${Constants.BASEURL}/task`).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(handleErrorObservable<any>('error in getTasksOfNewsItem'))
    );
  }

  getActionsOfAssessment(
    assessmentId: string,
    requirementId?: string
  ): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('assessment_id', assessmentId);
    if (requirementId) params = params.append('requirement_id', requirementId);
    return this.httpClient.get(`${Constants.BASEURL}/action`, { params }).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(handleErrorObservable<any>('error in getActionsOfAssessment'))
    );
  }

  getRequirementsOfAssessment(assessmentId: string): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('assessment_id', assessmentId);
    return this.httpClient
      .get(`${Constants.BASEURL}/requirement`, { params })
      .pipe(
        map((res: any) => {
          return res.data;
        }),
        catchError(
          handleErrorObservable<any>('error in getActionsOfAssessment')
        )
      );
  }

  getAssessmentById(id: string): Observable<any> {
    return this.httpClient
      .get(`${Constants.BASEURL}/assessment/${id}`)
      .pipe(
        catchError(handleErrorObservable<any>('error in getting assessment'))
      );
  }

  getActionById(id: string): Observable<any> {
    return this.httpClient
      .get(`${Constants.BASEURL}/action/${id}`)
      .pipe(catchError(handleErrorObservable<any>('error in getting action')));
  }

  getRequirementById(id: string): Observable<any> {
    return this.httpClient
      .get(`${Constants.BASEURL}/requirement/${id}`)
      .pipe(
        catchError(handleErrorObservable<any>('error in getting requirement'))
      );
  }

  changeActionStatus(actionId: string, statusId: string) {
    return this.httpClient.patch(`${Constants.BASEURL}/action/${actionId}`, {
      status_id: statusId,
    });
  }

  updateAssessment(assessment: Assessment) {
    return this.httpClient.put(
      `${Constants.BASEURL}/assessment/${assessment.id}`,
      assessment
    );
  }

  updateRequirement(requirement: Requirement) {
    return this.httpClient.put(
      `${Constants.BASEURL}/requirement/${requirement.id}`,
      requirement
    );
  }

  deleteRequirement(id: string) {
    return this.httpClient.delete(`${Constants.BASEURL}/requirement/${id}`);
  }

  updateAction(action: Action) {
    return this.httpClient.put(
      `${Constants.BASEURL}/action/${action.id}`,
      action
    );
  }

  deleteAction(id: string) {
    return this.httpClient.delete(`${Constants.BASEURL}/action/${id}`);
  }
  changeNewsItemWorkflowState(newsItemId: string, workflowStateId: number) {
    return this.httpClient.patch(
      `${Constants.BASEURL}/newsitem/${newsItemId}`,
      { workflow_state_id: workflowStateId }
    );
  }

  getAssessmentNoFormat(): Observable<any> {
    return this.httpClient
      .get(`${Constants.BASEURL}/assessment-no-format`)
      .pipe(
        map((res: any) => {
          return res.data;
        }),
        catchError(
          handleErrorObservable<any>('error in getting assessment format')
        )
      );
  }

  updateAssessmentNoFormat(formatId: string, format: string) {
    return this.httpClient.patch(
      `${Constants.BASEURL}/assessment-no-format/${formatId}`,
      { format }
    );
  }

  createAssessmentNoFormat(format: string) {
    return this.httpClient.post(`${Constants.BASEURL}/assessment-no-format/`, {
      format,
    });
  }

  createAssessmentProcess(assessmentId: string, processes: any[]) {
    const params = processes.map((id) => {
      return { assessment_id: assessmentId, process_id: id };
    });
    return this.httpClient.post(
      `${Constants.BASEURL}/assessment-process`,
      params
    );
  }

  getAllProcesses(): Observable<any> {
    return this.httpClient.get(`${Constants.BASEURL}/process`).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(handleErrorObservable<any>('error in getting process'))
    );
  }
}
