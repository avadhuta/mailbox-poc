import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Constants } from './../constants/constant';
import { handleErrorObservable } from '../utilities/helper';
import { SortField } from '../entities/sort-field';
import getDateInSqlFormat from '../utilities/sql-date-convertor';

@Injectable({
  providedIn: 'root',
})
export class ReportService {
  constructor(private httpClient: HttpClient) { }
  getAllNewsItems(
    limit: number,
    page: number,
    statusId?: string,
    sortByField?: SortField[],
    classification: string = null,
    startDate?: any,
    endDate?: any
  ): Observable<any> {
    let params: HttpParams = new HttpParams();
    params = params.append('$limit', '' + limit);
    params = params.append('$skip', '' + page * limit);
    params = params.append('startDate', startDate);
    params = params.append('endDate', endDate);
    if (statusId) {
      params = params.append('status_id', statusId);
    }
    if (sortByField && sortByField.length > 0) {
      sortByField.map(
        (field) =>
          (params = params.append(
            '$sort[' + field.name + ']',
            field.sortOrder === 'asc' ? '1' : '-1'
          ))
      );
    }
    if (classification) {
      params = params.append('classification', classification);
      return this.httpClient
        .get(`${Constants.BASEURL}/query-newsitems`, { params })
        .pipe(
          catchError(
            handleErrorObservable<any>(
              'error in getting news items of newsletter'
            )
          )
        );
    } else {
      return this.httpClient
        .get(`${Constants.BASEURL}/newsitem`, { params })
        .pipe(
          catchError(
            handleErrorObservable<any>('error in getAllInbox news items')
          )
        );
    }
  }

  getAllConfiguration() {
    return this.httpClient.get(`${Constants.BASEURL}/configuration`).pipe(
      map((res: any) => {
        console.log(res, 'res');
        return res;
      }),
      catchError(handleErrorObservable<any>('error in configuartion'))
    );
  }

  getAuditTrailReport(startDate: Date, endDate: Date, limit: number, page: number) {
    let params: HttpParams = new HttpParams();
    params = params.append('$limit', '' + limit);
    params = params.append('$skip', '' + page * limit);
    params = params.append('activity_date[$gte]', getDateInSqlFormat(startDate));
    params = params.append('activity_date[$lte]', getDateInSqlFormat(endDate));
    return this.httpClient.get(`${Constants.BASEURL}/audit-trail`, { params }).pipe(
      catchError(handleErrorObservable<any>('error in getting audit reports'))
    );
  }
}
