import { Injectable } from '@angular/core';
import { Message } from '../model/message';
import { Observable, of, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Constants } from './../constants/constant';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  selectedMessage = null;
  deletedMessage = null;
  messageStack = [];
  private isSelectedMessageChangedSource = new Subject<any>();
  isSelectedMessageChangedObservable = this.isSelectedMessageChangedSource.asObservable();

  private isMessageDeletedChangedSource = new Subject<any>();
  isMessageDeletedChangedObservable = this.isMessageDeletedChangedSource.asObservable();

  constructor(private httpClient: HttpClient) {}

  getNewMails(): Observable<any> {
    return this.httpClient.get(`${Constants.BASEURL}/email/mail`);
  }

  onPushToMessageStack() {
    this.httpClient
      .get<any>(`${Constants.BASEURL}/email/mail`)
      .subscribe((incomingMessages) => {
        if (incomingMessages && incomingMessages.length > 0) {
          window.alert('You have received a new mail');
          incomingMessages.map((msg) => {
            const incomingMessage = new Message();
            (incomingMessage.id = msg.uid),
              // incomingMessage.mail_to = msg.to[0].address,
              // incomingMessage.avatarUrl = 'http://api.randomuser.me/portraits/lego/4.jpg',
              // incomingMessage.mail_from = msg.from[0].name,
              // incomingMessage.date = msg.date,
              // incomingMessage.email = msg.from[0].address,
              // incomingMessage.subject = msg.subject,
              // incomingMessage.content = msg.text,
              // incomingMessage.attachment = 'https://picsum.photos/200',
              // incomingMessage.set_to = ['inbox'],
              this.messageStack.push(incomingMessage);
          });
        }
      });
  }

  setDeletedMessage(selectedMessage) {
    this.deletedMessage = selectedMessage;
    this.isMessageDeletedChangedSource.next(selectedMessage);
    console.log('set deleted message', this.deletedMessage);
  }

  setSelectedMessage(selectedMessage) {
    this.selectedMessage = selectedMessage;
    this.isSelectedMessageChangedSource.next(selectedMessage);
  }

  getSelectedMessage() {
    return this.selectedMessage;
  }

  deleteSelectedMessage(selectedMessageId) {
    this.setDeletedMessage(selectedMessageId);
    const index = this.messageStack.findIndex(
      (msg) => msg.id === selectedMessageId
    );
    this.messageStack.splice(index, 1);
    this.isMessageDeletedChangedSource.next(selectedMessageId);
    return this.messageStack;
  }

  getMessages(): Observable<Message[]> {
    return of(this.messageStack);
  }

  getMessagesByEmailID(email): Observable<Message[]> {
    return of(
      this.messageStack.filter((messages) => messages.mail_to === email)
    );
  }

  getMessagesFromInbox(email, state): Observable<Message[]> {
    return of(
      this.messageStack.filter(
        (messages) =>
          messages.set_to.indexOf(state) !== -1 && messages.mail_to === email
      )
    );
  }
}
