import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Constants } from '../constants/constant';
import { handleErrorObservable } from '../utilities/helper';

@Injectable({
  providedIn: 'root',
})
export class InboxService {
  constructor(private httpClient: HttpClient) { }

  getAllInboxMail(): Observable<any> {
    return this.httpClient.get(`${Constants.BASEURL}/mail`).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(handleErrorObservable<any>('error in getAllInbox Mail'))
    );
  }

  archiveMail(mailId: string): Observable<any> {
    return this.httpClient.post(`${Constants.BASEURL}/archive-mail`, { mailId }).pipe(
      catchError(handleErrorObservable<any>('error in getAllInbox Mail'))
    );
  }
}
