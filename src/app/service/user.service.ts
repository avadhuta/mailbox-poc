import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from '../model/user';
import { handleErrorObservable } from '../utilities/helper';
import { Constants } from './../constants/constant';
import { AuthTokenService } from './auth-token.service';


@Injectable({
  providedIn: 'root',
})
export class UserService {
  private currentUserChangedSource = new Subject<User>();
  currentUserChanged = this.currentUserChangedSource.asObservable();
  currentUser: User;

  constructor(
    private httpClient: HttpClient,
    private tokenService: AuthTokenService,
    private router: Router
  ) { }

  login(email: string, password: string) {
    const url = `${Constants.BASEURL}/authentication`;
    let params: HttpParams = new HttpParams();
    params = params.set('strategy', 'local');
    params = params.set('email', email);
    params = params.set('password', password);
    return this.httpClient.post(url, params).pipe(
      map((res: any) => {
        console.log("UserService -> login -> res", res);
        this.setCurrentUser(res.user);
        this.tokenService.setToken(res.accessToken);
        return res;
      }),
      catchError(handleErrorObservable<any>('user Login'))
    );
  }

  isLoggedIn(): boolean {
    return this.currentUser != null;
  }

  setCurrentUser(user) {
    const parsedUser = JSON.stringify(user);
    localStorage.setItem('user', parsedUser);
    this.currentUser = user;
    this.currentUserChangedSource.next(user);
  }

  getCurrentUser(): User {
    return this.currentUser;
  }
  // get current firebase collectio data
  getCurrentUserData(): Observable<any> {
    if (this.currentUser) {
      return of(this.currentUser);
    } else {
      return this.currentUserChanged;
    }
  }

  logout() {
    console.log(UserService.name, 'logging out user');
    this.tokenService.clear();
    this.setCurrentUser(null);
  }

  checkUser(forceRefresh = false): Observable<boolean> {
    if (!forceRefresh && this.currentUser != null) {
      return of(true);
    }
    const localToken = this.tokenService.getToken();
    if (!localToken) {
      return of(false);
    }
  }

  getAllUsers() {
    let params: HttpParams = new HttpParams();
    params = params.append('$limit', "2000");
    return this.httpClient.get(`${Constants.BASEURL}/users`, { params }).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(handleErrorObservable<any>('error in getting users'))
    );
  }

  getAllRoles() {
    let params: HttpParams = new HttpParams();
    params = params.append('$limit', "2000");
    return this.httpClient.get(`${Constants.BASEURL}/role`, { params }).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(handleErrorObservable<any>('error in getting roles'))
    );
  }

  getRolesForWorkflow(workflowId: string) {
    let params: HttpParams = new HttpParams();
    params = params.append('workflow_state_id', workflowId);
    return this.httpClient.get(`${Constants.BASEURL}/workflow-roles`, { params }).pipe(
      map((res: any) => {
        return res.data;
      }),
      catchError(handleErrorObservable<any>('error in getting workflow roles'))
    );
  }


}
