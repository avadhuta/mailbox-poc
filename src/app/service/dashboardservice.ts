import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from '../constants/constant';

@Injectable({
  providedIn: 'root',
})
export class DashboardServiceService {
  constructor(private httpClient: HttpClient) {}

  getAllInboxMail(): Observable<any> {
    return this.httpClient.get(`${Constants.BASEURL}/email/mail`);
  }
}
