import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { AssessmentDialogComponent } from '../assessment-dialog/assessment-dialog.component';
import { AssessmentService } from 'src/app/service/assessment.service';
import { Assessment } from 'src/app/entities/assessment';
import { Requirement } from 'src/app/entities/requirement';
import { NewsItem } from 'src/app/entities/newsitem';
import { MatTableDataSource } from '@angular/material/table';
import { MatBottomSheet } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-assessment-details',
  templateUrl: './assessment-details.component.html',
  styleUrls: ['./assessment-details.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class AssessmentDetailsComponent implements OnInit {
  newsItemId: string;
  assessment: Assessment;
  newsItem: NewsItem;
  actionsCount: number = 0;
  requirements: Requirement[] = [];
  displayedColumns: string[] = ['details', 'impact', 'edit', 'delete', 'actions',];
  dataSource: MatTableDataSource<Requirement>;
  constructor(
    private _bottomSheet: MatBottomSheet,
    private router: Router,
    public dialog: MatDialog,
    private assessmentService: AssessmentService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.requirements);
    this.route.queryParams.subscribe((params) => {
      this.newsItemId = params['newsitem'];
      this.getAssessmentOfNewsItem();
    });
  }

  onViewActions(requirementId?: string) {
    console.log('assessment', this.assessment);
    const params = { assessment: this.assessment.id };
    if (requirementId) {
      params['requirement'] = requirementId;
    }
    this.router.navigate(['/actions'], { queryParams: params });
  }

  downLoadFile(file) {
    if (file) {
      window.open(file, '_blank');
    }
  }

  getRequirements(assessmentId: string) {
    this.assessmentService
      .getRequirementsOfAssessment(assessmentId)
      .subscribe((res) => {
        this.requirements = res;
        this.dataSource.data = this.requirements;
      });
  }

  getAssessmentOfNewsItem() {
    this.assessmentService
      .getAssessmentOfNewsItem(this.newsItemId)
      .subscribe((item) => {
        console.log(
          'AssessmentDetailsComponent -> getAssessmentOfNewsItem -> item',
          item
        );
        this.assessment = item;
        console.log(this.assessment, 'assessment');
        this.getRequirements(this.assessment.id);
        this.getActionsCount(this.assessment.id);
      });
  }

  getActionsCount(id) {
    this.assessmentService
      .getActionsOfAssessment(id, null)
      .subscribe((actions) => {
        if (actions) {
          this.actionsCount = actions.length;
          console.log(this.actionsCount, 'len');
        }
      });
  }

  createRequirement(value: any) {
    const req: Requirement = {
      id: '',
      details: value.requirement,
      impact: value.impact,
      assessment_id: this.assessment.id,
    };
    this.assessmentService.createRequirement(req).subscribe((req) => {
      console.log(
        'AssessmentDetailsComponent -> createRequirement -> req',
        req
      );
      this.getRequirements(this.assessment.id);
    });
  }

  updateRequirement(id: string, value: any) {
    const req: Requirement = {
      id,
      details: value.requirement,
      impact: value.impact,
      assessment_id: this.assessment.id,
    };
    console.log(req, 'req');
    this.assessmentService.updateRequirement(req).subscribe((req) => {
      console.log(
        'AssessmentDetailsComponent -> createRequirement -> updated',
        req
      );
      this.getRequirements(this.assessment.id);
    });
  }

  onEdit(data) {
    console.log(data, 'id');
    const bottomSheetRef = this._bottomSheet.open(AssessmentDialogComponent, {
      disableClose: true,
      panelClass: 'custom-bottom-sheet-width',
      data,
    });
    bottomSheetRef.afterDismissed().subscribe((result) => {
      if (result) {
        this.updateRequirement(data.id, result);
      }
    });
  }

  onDelete(id) {
    this.assessmentService.deleteRequirement(id).subscribe((req) => {
      console.log(
        'AssessmentDetailsComponent -> createRequirement -> deleted',
        req
      );
      this.getRequirements(this.assessment.id);
    });
  }

  onAddAssessment() {
    const bottomSheetRef = this._bottomSheet.open(AssessmentDialogComponent, {
      disableClose: true,
      panelClass: 'custom-bottom-sheet-width',
    });
    bottomSheetRef.afterDismissed().subscribe((result) => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.createRequirement(result);
      }
    });
  }
}
