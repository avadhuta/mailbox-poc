import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelevantFormComponent } from './relevant-form.component';

describe('RelevantFormComponent', () => {
  let component: RelevantFormComponent;
  let fixture: ComponentFixture<RelevantFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelevantFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelevantFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
