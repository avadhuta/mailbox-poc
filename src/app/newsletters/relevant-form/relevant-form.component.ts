import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NewsletterService } from 'src/app/service/newsletter.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Task } from 'src/app/entities/task';
import { AssessmentService } from 'src/app/service/assessment.service';
import { Classification } from 'src/app/entities/classification';
import { UserService } from 'src/app/service/user.service';
import { User } from 'src/app/model/user';
import { MyErrorStateMatcher } from 'src/app/utilities/error-state-matcher';
import { Assessment } from 'src/app/entities/assessment';
import { TaskStatus } from 'src/app/entities/task-status.enum';
import { Process } from 'src/app/entities/process';
import countriesList from '../../utilities/countries';
import { colorSets } from '@swimlane/ngx-charts';
import { NewsItem } from 'src/app/entities/newsitem';

interface WorkFlow {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-relevant-form',
  templateUrl: './relevant-form.component.html',
  styleUrls: ['./relevant-form.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class RelevantFormComponent implements OnInit {
  newsItemId: string;
  newsItem: NewsItem;
  workflowId: string;
  relevantForm: FormGroup;
  assessmentList: Classification[] = [];
  users: User[] = [];
  matcher;
  minDate: Date;
  maxDate: Date;
  filteredOptions: Observable<User[]>;
  myControl = new FormControl();
  assessment: Assessment;
  processes: Process[] = [];
  countries = countriesList;
  uploadedUrl = '';

  workflows: WorkFlow[] = [
    { value: 'workflow-0', viewValue: 'Initiation' },
    { value: 'workflow-1', viewValue: 'Assessment' },
    { value: 'workflow-2', viewValue: 'Review' },
    { value: 'workflow-3', viewValue: 'Approval' },
    { value: 'workflow-4', viewValue: 'Implementation' },
    { value: 'workflow-5', viewValue: 'Closure' },
  ];

  constructor(
    private router: Router,
    private location: Location,
    private newsletterService: NewsletterService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private assessmentService: AssessmentService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.relevantForm = new FormGroup({
      assessments: new FormControl(''),
      // intiation: new FormControl(),
      start_date: new FormControl('', Validators.required),
      end_date: new FormControl('', Validators.required),
      assign_user: new FormControl('', Validators.required),
      comments: new FormControl('', Validators.required),
      countries_impacted: new FormControl('', Validators.required),
      region_impacted: new FormControl('', Validators.required),
      process: new FormControl('', Validators.required),
      document_url: new FormControl(''),
    });
    this.matcher = new MyErrorStateMatcher();

    this.getAllProcesses();

    this.getClassifications();
    this.userService.getAllUsers().subscribe((users) => {
      this.users = users;
      // filter users with specific permissions
      if (this.workflowId) {
        this.filterUsersByWorkflow();
      } else {
        this.filteredOptions = of(this.users);
      }
    });
    this.route.queryParams.subscribe((params) => {
      this.newsItemId = params['newsitem'];
      this.workflowId = params['workflow'];
      if (this.newsItemId) {
        this.getNewsItemById(this.newsItemId);
      }
      if (!this.workflowId) {
        this.relevantForm.get('assessments').setValidators(Validators.required);
      }
    });

    this.filteredOptions = this.relevantForm.controls.assign_user.valueChanges.pipe(
      map((value) => this._filter(value))
    );
  }

  getNewsitemProcess(id) {
    this.newsletterService.getNewsitemProcess(id).subscribe((process) => {
      if (process) {
        const arrayOfProcessId = process.map((item) => item.process_id);
        console.log(arrayOfProcessId, 'arrayOfProcessId');
        this.relevantForm.controls['process'].setValue(
          arrayOfProcessId
        );
      }
    });
  }

  getNewsItemById(id: string) {
    this.newsletterService.getNewsItemById(id).subscribe((newsItem) => {
      if (newsItem) {
        console.log(newsItem, 'newsItem');
        this.getNewsitemProcess(newsItem.id);
        this.newsItem = newsItem;
        // this.relevantForm.controls['function_impacted'].setValue(
        //   this.newsItem['function_impacted']
        // );
        this.relevantForm.controls['region_impacted'].setValue(
          this.newsItem['region_impacted']
        );
        this.relevantForm.controls['countries_impacted'].setValue(
          this.newsItem['country_impacted']
        );
        console.log(
          'NewsItemInDetailComponent -> getNewsItemById -> newsItem',
          this.newsItem
        );
      }
    });
  }

  onFileUploadComplete(uploadedUrl) {
    this.uploadedUrl = uploadedUrl;
  }

  removeUploadedFile() {
    this.uploadedUrl = '';
  }

  getAllProcesses() {
    this.assessmentService
      .getAllProcesses()
      .subscribe((process) => (this.processes = process));
  }

  filterUsersByWorkflow() {
    this.userService.getRolesForWorkflow(this.workflowId).subscribe((roles) => {
      this.users = this.users.filter((user) => {
        console.log('filtering users', user.roles, roles);
        return roles.some(
          (role) => user.roles.findIndex((ur) => ur === role.role_id) !== -1
        );
      });
      console.log('filtered users', this.users);
      this.filteredOptions = of(this.users);
    });
  }

  compareTwoDates() {
    this.minDate = this.relevantForm.controls['start_date'].value;
    this.getAssessmentOfNewsItem();
  }

  validateEndDate() {
    if (!this.minDate) {
      return true;
    }
    if (this.assessment) {
      this.maxDate = this.assessment.assessment_due_date;
    }
  }

  getAssessmentOfNewsItem() {
    if (this.newsItemId) {
      this.assessmentService
        .getAssessmentOfNewsItem(this.newsItemId)
        .subscribe((item) => {
          console.log(
            'RelevantFormComponent -> getAssessmentOfNewsItem -> item',
            item
          );
          this.assessment = item;
        });
    }
  }

  displayUserName(user: User): string {
    console.log('RelevantFormComponent -> displayUserName -> user', user);

    return user && user.name ? user.name : '';
  }

  getClassifications() {
    this.newsletterService
      .getNewsItemClassifications()
      .subscribe((classes) => (this.assessmentList = classes));
  }

  private _filter(value: string): User[] {
    const filterValue = value.toLowerCase();
    console.log('RelevantFormComponent -> filterValue', filterValue);

    return this.users.filter((user) =>
      user.name.toLowerCase().includes(filterValue)
    );
  }

  onBackClick() {
    this.location.back();
  }

  createTask(values: any): Observable<any> {
    const task: Task = {
      id: '',
      comments: values.comments,
      created_date: new Date(),
      due_date: values.end_date,
      start_date: values.start_date,
      user_assigned: values.assign_user.id,
      newsitem_id: this.newsItemId,
      status_id: TaskStatus.Active,
      workflow_state_id: +this.workflowId,
      document_url: values.document_url,
    };
    return this.assessmentService.createTask(task);
  }

  addNewsitemCountryAndProcessImpacted(
    newsitemId: string,
    process: string[],
    country: string,
    region: string
  ) {
    this.newsletterService
      .createNewsitemProcess(newsitemId, process)
      .subscribe((res) => {
        this.newsletterService
          .changeNewsItemCountryImpacted(newsitemId, country)
          .subscribe((countryChanged) => {
            console.log(
              'RelevantFormComponent -> addNewsitemCountryAndProcessImpacted -> countryChanged',
              countryChanged
            );
          });
      });
    this.newsletterService
      .changeNewsItemRegionImpacted(newsitemId, region)
      .subscribe((region) => {
        console.log(
          '🚀 ~ file: relevant-form.component.ts ~ line 221 ~ RelevantFormComponent ~ .subscribe ~ region',
          region
        );
      });
  }

  closeCurrentUserTask() {
    console.log("finding current user task to close");
    this.newsletterService.getTasksOfNewsItem(this.newsItemId).subscribe((tasks: Task[]) => {
      console.log("RelevantFormComponent -> closeCurrentUserTask -> tasks", tasks);
      const currentUserActiveTask = tasks.find(task => task.user_assigned === this.userService.getCurrentUser().id && task.status_id === 1);
      console.log("RelevantFormComponent -> closeCurrentUserTask -> currentUserActiveTask", currentUserActiveTask);
      if (currentUserActiveTask) {
        this.newsletterService.completeTask(currentUserActiveTask.id).subscribe(() => {
          console.log("closed current user task");
        });
      }
    })
  }

  onSubmit() {
    console.log("submitting form....");
    this.relevantForm.controls['document_url'].setValue(this.uploadedUrl);
    console.log('checking form', this.relevantForm.value);
    // change status to relevant
    if (this.workflowId) {
      this.newsletterService
        .changeNewsItemWorkflowState(this.newsItemId, +this.workflowId)
        .subscribe((res) => {
          console.log('RelevantFormComponent -> onSubmit -> res', res);
          this.closeCurrentUserTask();
          this.createTask(this.relevantForm.value).subscribe((result) => {
            console.log('new task created', result);
            this.snackBar.open('Newsletter marked as relevant', null, {
              duration: 2000,
            });
            this.router.navigate(['/newsitems/' + this.newsItemId]);
          });
        });
    } else {
      this.newsletterService
        .changeNewsItemStatus(this.newsItemId, '1')
        .subscribe((res) => {
          console.log(
            'RelevantFormComponent -> onSubmit -> changeNewsItemStatus',
            res
          );
          this.createTask(this.relevantForm.value).subscribe(() => {
            this.newsletterService
              .changeNewsItemClassification(
                this.newsItemId,
                this.relevantForm.value.assessments
              )
              .subscribe((resp) => {
                console.log('changed news item classification', res);
                this.snackBar.open('Newsletter marked as relevant', null, {
                  duration: 2000,
                });
                this.router.navigate(['/newsitems/' + this.newsItemId]);
              });
          });
        });
      this.addNewsitemCountryAndProcessImpacted(
        this.newsItemId,
        this.relevantForm.value.process,
        this.relevantForm.value.countries_impacted,
        this.relevantForm.value.region_impacted
      );
    }
  }
}
