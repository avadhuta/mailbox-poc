import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { AddActionDialogComponent } from '../add-action-dialog/add-action-dialog.component';
import { AssessmentService } from 'src/app/service/assessment.service';
import { Action } from 'src/app/entities/action';
import { MatTableDataSource } from '@angular/material/table';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import {
  ConfirmDialogModel,
  ConfirmDialogComponent,
} from 'src/app/utilities/confirm-dialog/confirm-dialog.component';
import { TaskStatus } from 'src/app/entities/task-status.enum';
interface Actions {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class ActionsComponent implements OnInit {
  assessmentId: string;
  requirementId: string;
  actions: Actions[] = [
    { value: 'action-0', viewValue: 'Active' },
    { value: 'action-2', viewValue: 'Completed' },
  ];
  displayedColumns: string[] = [
    'description',
    'assigned_user_id',
    'due_date',
    'evidence_file_url',
    'edit',
    'delete',
    'status_id',
  ];
  dataSource: MatTableDataSource<Actions>;

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private assessmentService: AssessmentService,
    private _bottomSheet: MatBottomSheet
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.actions);
    this.route.queryParams.subscribe((params) => {
      this.assessmentId = params['assessment'];
      this.requirementId = params['requirement'];
      console.log(this.requirementId, 'this.requirementId');
    });
    this.getActions();
  }

  onNavigateTo() {
    this.router.navigate(['/actions']);
  }

  getActions() {
    this.assessmentService
      .getActionsOfAssessment(this.assessmentId, this.requirementId)
      .subscribe((actions) => {
        this.actions = actions;
        console.log(this.actions, 'actions');
        this.dataSource.data = this.actions;
      });
  }

  downLoadFile(file) {
    if (file) {
      window.open(file, '_blank');
    }
  }

  createAction(value: any) {
    const action: Action = {
      id: '',
      assessment_id: this.assessmentId,
      description: value.description,
      assigned_user_id: value.assigned_user.id,
      due_date: value.due_date,
      evidence_file_url: value.evidence_file_url,
      requirement_id: this.requirementId,
      status_id: 1,
    };
    this.assessmentService.createAction(action).subscribe((res) => {
      console.log('ActionsComponent -> createAction -> res', res);
      this.getActions();
    });
  }

  updateAction(id: string, value: any) {
    const action: Action = {
      id,
      assessment_id: this.assessmentId,
      description: value.description,
      assigned_user_id: value.assigned_user.id,
      due_date: value.due_date,
      evidence_file_url: value.evidence_file_url,
      requirement_id: this.requirementId,
      status_id: 1,
    };
    console.log(action, 'action');
    this.assessmentService.updateAction(action).subscribe((res) => {
      console.log('ActionsComponent -> createAction -> update', res);
      this.getActions();
    });
  }

  edit(data) {
    console.log(data, 'data');
    const bottomSheetRef = this._bottomSheet.open(AddActionDialogComponent, {
      disableClose: true,
      panelClass: 'custom-bottom-sheet-width',
      data,
    });
    bottomSheetRef.afterDismissed().subscribe((result) => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.updateAction(data.id, result);
      }
    });
  }

  delete(id) {
    this.assessmentService.deleteAction(id).subscribe(() => {
      console.log('deleted sucessfuy');
      this.getActions();
    });
  }

  completeTask(taskId: string) {
    const dialogData = new ConfirmDialogModel(
      'Confirm Action',
      'This will complete the action, are you sure ?'
    );
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      console.log(
        'NewsItemInDetailComponent -> onChangeStatus -> dialogResult',
        dialogResult
      );
      if (dialogResult) {
        this.assessmentService
          .changeActionStatus(taskId, '' + TaskStatus.Completed)
          .subscribe((res) => {
            console.log('ActionsComponent -> completeTask -> res', res);
            this.getActions();
          });
      }
    });
  }

  isTaskComplete(statusId: number) {
    return statusId ? statusId === TaskStatus.Completed : false;
  }

  onAddAction() {
    const bottomSheetRef = this._bottomSheet.open(AddActionDialogComponent, {
      disableClose: true,
      panelClass: 'custom-bottom-sheet-width',
    });
    bottomSheetRef.afterDismissed().subscribe((result) => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.createAction(result);
      }
    });
  }

  onStatusChange(event) {
    console.log('action event', event);
  }
}
