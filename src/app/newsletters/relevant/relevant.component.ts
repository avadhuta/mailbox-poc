import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { NewsletterService } from 'src/app/service/newsletter.service';
import { NewsItem } from 'src/app/entities/newsitem';
import { SortField } from 'src/app/entities/sort-field';
import { NewsitemsDataSource } from 'src/app/utilities/data-table-source';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-relevant',
  templateUrl: './relevant.component.html',
  styleUrls: ['./relevant.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RelevantComponent implements OnInit {
  @Input() allNewItemList;
  @Input() isActive;

  displayedColumns: string[] = ['date', 'title', 'status_id', 'newsletter_id'];
  dataSource: NewsitemsDataSource;
  selectedClassification = 'ALL';
  selectedClassificationId: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  newsItems: NewsItem[];
  limit: number;
  page: number;
  total: number;
  newsletterId: string;
  sortField: SortField = {
    name: "received_date",
    sortOrder: "-1"
  }

  constructor(private router: Router, private route: ActivatedRoute, private newsletterService: NewsletterService) {
    this.limit = 200;
    this.page = 0;
  }

  ngOnChanges() {
    console.log('ngOnChanges -> isActive', this.isActive);
    if (this.isActive === true) {
      this.initNotRelevantData();
    }
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        tap(() => this.loadNewsitemsPage())
      )
      .subscribe();
  }

  loadNewsitemsPage() {
    this.dataSource.loadNewsitems(
      '1',
      this.newsletterId,
      this.sortField,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.selectedClassificationId);
  }

  applyFilter(event: Event) {
    // const filterValue = (event.target as HTMLInputElement).value;
    // this.dataSource.filter = filterValue.trim().toLowerCase();

    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  getClassificationString(newsItem: any) {
    let classification = "";
    newsItem.classification && newsItem.classification.map(item => {
      if (item == 1) classification = classification + "IA, ";
      if (item == 2) classification = classification + "CQ, ";
      if (item == 3) classification = classification + "NL, ";
    });
    classification = classification.substr(0, classification.length - 2);
    return classification;
  }

  ngOnInit(): void {
    this.dataSource = new NewsitemsDataSource(this.newsletterService);
    this.route.queryParams.subscribe(params => {
      console.log('UnclassifiedComponent -> ngOnInit -> params', params);
      this.newsletterId = params['newsletter'];
      this.initNotRelevantData();
    });
  }

  initNotRelevantData() {
    if (this.newsletterId) {
      this.filterNewsItemsOfNewsletter(this.newsletterId);
      this.dataSource.loadNewsitems("1", this.newsletterId, this.sortField, this.page, this.limit, this.selectedClassificationId);
    } else {
      this.getAllNewsItems();
      this.dataSource.loadNewsitems("1", this.newsletterId, this.sortField, this.page, this.limit, this.selectedClassificationId);
    }
  }
  changeClassification(classification: string) {
    this.selectedClassification = classification;
    const classId = classification === 'IA' ? 1 : (classification === "CQ" ? 2 : (classification === "NL" ? 3 : -1));
    this.selectedClassificationId = classId === -1 ? null : "" + classId;
    this.dataSource.loadNewsitems("1", this.newsletterId, this.sortField, this.page, this.limit, this.selectedClassificationId);
    // if (classId !== -1) {
    //   this.filterByClassification(classId);
    // } else {
    //   //this.dataSource.data = this.newsItems;
    // }
  }

  filterByClassification(classificationId: number) {
    //this.dataSource.data = this.newsItems.filter(item => item.classification.indexOf(classificationId) !== -1);
  }

  filterNewsItemsOfNewsletter(newsletterId: string) {
    console.log("filtering news items of ", newsletterId);
    this.newsletterService.getNewsItemsOfNewsletter(newsletterId, "1", this.limit, this.page)
      .subscribe(items => {
        //this.newsItems = items;
        this.total = items.total;
        //this.dataSource.data = this.newsItems;
      });
  }

  getAllNewsItems() {
    const sortField: SortField = {
      name: "received_date",
      sortOrder: "-1"
    }
    this.newsletterService.getAllNewsItems(this.limit, this.page, "1", [sortField]).subscribe((items) => {
      if (items) {
        console.log("AllNewsItemsComponent -> getAllNewsItems -> items", items);
        //this.newsItems = items.data;
        this.total = items.total;
        //this.dataSource.data = this.newsItems;
      }
    });
  }

  navigateTo(item) {
    this.router.navigate(['/newsitems/' + item.id]);
  }
}
