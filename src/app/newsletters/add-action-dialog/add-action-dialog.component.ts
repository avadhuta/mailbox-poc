import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/model/user';
import { UserService } from 'src/app/service/user.service';
import {
  MatBottomSheetRef,
  MAT_BOTTOM_SHEET_DATA,
} from '@angular/material/bottom-sheet';
import { MyErrorStateMatcher } from 'src/app/utilities/error-state-matcher';

@Component({
  selector: 'app-add-action-dialog',
  templateUrl: './add-action-dialog.component.html',
  styleUrls: ['./add-action-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class AddActionDialogComponent implements OnInit {
  addActionForm: FormGroup;
  users: User[] = [];
  matcher;
  filteredOptions: Observable<User[]>;
  myControl = new FormControl();
  uploadedUrl = '';

  constructor(
    public _bottomSheetRef: MatBottomSheetRef<AddActionDialogComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public editActionData,
    private userService: UserService
  ) {
    console.log('edit data', this.editActionData);
    if (this.editActionData) {
      this.uploadedUrl =
        this.editActionData && this.editActionData.evidence_file_url
          ? this.editActionData.evidence_file_url
          : '';
      console.log(this.uploadedUrl, 'file_url');
    }
  }

  ngOnInit(): void {
    this.addActionForm = new FormGroup({
      description: new FormControl(
        this.editActionData && this.editActionData.description
          ? this.editActionData.description
          : '',
        Validators.required
      ),
      assigned_user: new FormControl(
        this.editActionData && this.editActionData.user
          ? this.editActionData.user
          : '',
        Validators.required
      ),
      due_date: new FormControl(
        this.editActionData && this.editActionData.due_date
          ? new Date(this.editActionData.due_date)
          : '',
        Validators.required
      ),
      evidence_file_url: new FormControl(
        this.editActionData && this.editActionData.evidence_file_url
          ? this.editActionData.evidence_file_url
          : ''
      ),
      // status: new FormControl('', Validators.required)
    });
    this.matcher = new MyErrorStateMatcher();

    this.filteredOptions = this.addActionForm.controls.assigned_user.valueChanges.pipe(
      map((value) => this._filter(value))
    );

    this.userService.getAllUsers().subscribe((users) => {
      this.users = users;
      this.filteredOptions = of(this.users);
      console.log('RelevantFormComponent -> ngOnInit -> users', users);
    });
  }

  onFileUploadComplete(uploadedUrl) {
    this.uploadedUrl = uploadedUrl;
  }

  removeUploadedFile() {
    this.uploadedUrl = '';
  }

  displayUserName(user: User): string {
    console.log('RelevantFormComponent -> displayUserName -> user', user);
    return user && user.name ? user.name : '';
  }

  private _filter(value: string): User[] {
    const filterValue = value.toLowerCase();
    return this.users.filter((user) =>
      user.name.toLowerCase().includes(filterValue)
    );
  }

  onSubmit() {
    const actionValues = this.addActionForm.value;
    actionValues['evidence_file_url'] = this.uploadedUrl;
    console.log(this.addActionForm.value);
    this.addActionForm.controls['evidence_file_url'].setValue(this.uploadedUrl);
    this._bottomSheetRef.dismiss(this.addActionForm.value);
  }
}
