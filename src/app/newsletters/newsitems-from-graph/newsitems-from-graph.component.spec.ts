import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsitemsFromGraphComponent } from './newsitems-from-graph.component';

describe('NewsitemsFromGraphComponent', () => {
  let component: NewsitemsFromGraphComponent;
  let fixture: ComponentFixture<NewsitemsFromGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsitemsFromGraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsitemsFromGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
