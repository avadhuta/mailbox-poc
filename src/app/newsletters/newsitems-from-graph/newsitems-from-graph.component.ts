import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { NewsItem } from 'src/app/entities/newsitem';
import { SortField } from 'src/app/entities/sort-field';
import { NewsletterService } from 'src/app/service/newsletter.service';
import { GraphNewsitemsDataSource } from 'src/app/utilities/datasource-newsitems-graph';

@Component({
  selector: 'app-newsitems-from-graph',
  templateUrl: './newsitems-from-graph.component.html',
  styleUrls: ['./newsitems-from-graph.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class NewsitemsFromGraphComponent implements OnInit {
  displayedColumns: string[] = ['date', 'title', 'newsletter_id', 'status_id'];
  dataSource: GraphNewsitemsDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  newsItems: NewsItem[];
  limit: number;
  page: number;
  total: number;
  sortField: SortField = {
    name: "received_date",
    sortOrder: "-1"
  };
  processId: string;
  receivedDate: Date;
  country: string;

  constructor(private router: Router, private route: ActivatedRoute, private newsletterService: NewsletterService) {
    this.limit = 200;
    this.page = 0;
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(tap(() => this.loadNewsitemsPage())).subscribe();
  }

  loadNewsitemsPage() {
    this.dataSource.loadNewsitems(this.processId, this.country, this.receivedDate, this.paginator?.pageIndex,
      this.paginator?.pageSize);
  }

  applyFilter(event: Event) {
  }

  ngOnInit(): void {
    this.dataSource = new GraphNewsitemsDataSource(this.newsletterService);
    this.route.queryParams.subscribe(params => {
      console.log('UnclassifiedComponent -> ngOnInit -> params', params);
      this.processId = params["processId"];
      this.country = params["country"];
      this.receivedDate = params["receivedDate"] ? new Date(params["receivedDate"]) : new Date();
      this.loadNewsitemsPage();
      console.log("NewsitemsFromGraphComponent -> ngOnInit -> this.receivedDate", this.receivedDate, this.country, this.processId)
    });
  }

  navigateTo(event, item) {
    console.log("NewsitemsFromGraphComponent -> navigateTo -> event", event);
    event.stopPropagation();
    this.router.navigate(['/newsitems/' + item.id]);
  }

  onViewNewsletter(event, newsitem) {
    event.stopPropagation();
    this.router.navigate(['/newsitems'], {
      queryParams: { newsletter: newsitem.newsletter_id.id },
    });
  }
}

