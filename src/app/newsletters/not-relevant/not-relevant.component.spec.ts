import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotRelevantComponent } from './not-relevant.component';

describe('NotRelevantComponent', () => {
  let component: NotRelevantComponent;
  let fixture: ComponentFixture<NotRelevantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotRelevantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotRelevantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
