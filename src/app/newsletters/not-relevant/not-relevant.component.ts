import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NewsItem } from 'src/app/entities/newsitem';
import { NewsletterService } from 'src/app/service/newsletter.service';
import { Newsletter } from 'src/app/entities/newsletter';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SortField } from 'src/app/entities/sort-field';
import { NewsitemsDataSource } from 'src/app/utilities/data-table-source';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-not-relevant',
  templateUrl: './not-relevant.component.html',
  styleUrls: ['./not-relevant.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class NotRelevantComponent implements OnInit {
  @Input() isActive;
  displayedColumns: string[] = ['date', 'title', 'newsletter_id'];
  dataSource: NewsitemsDataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  newsItems: NewsItem[];
  limit: number;
  total: number;
  page: number;
  newsLetter: Newsletter;
  newsletterId: string;
  sortField: SortField = {
    name: "received_date",
    sortOrder: "-1"
  }

  constructor(
    private newsletterService: NewsletterService, private router: Router, private route: ActivatedRoute) {
    this.limit = 200;
    this.page = 0;
  }

  ngOnChanges() {
    console.log('ngOnChanges -> isActive', this.isActive);
    if (this.isActive === true) {
      this.initNotRelevantData();
    }
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit - > isActive', this.isActive);

    this.paginator.page
      .pipe(
        tap(() => this.loadNewsitemsPage())
      )
      .subscribe();
  }

  loadNewsitemsPage() {
    this.dataSource.loadNewsitems(
      '2',
      this.newsletterId,
      this.sortField,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  applyFilter(event: Event) {
    // const filterValue = (event.target as HTMLInputElement).value;
    // this.dataSource.filter = filterValue.trim().toLowerCase();

    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  ngOnInit(): void {
    this.dataSource = new NewsitemsDataSource(this.newsletterService);
    this.route.queryParams.subscribe(params => {
      console.log('UnclassifiedComponent -> ngOnInit -> params', params);
      this.newsletterId = params['newsletter'];
      this.initNotRelevantData();
    });
  }

  initNotRelevantData() {
    if (this.newsletterId) {
      this.filterNewsItemsOfNewsletter(this.newsletterId);
      this.dataSource.loadNewsitems("2", this.newsletterId, this.sortField, this.page, this.limit);
    } else {
      this.getAllNewsItems();
      this.dataSource.loadNewsitems("2", this.newsletterId, this.sortField, this.page, this.limit);
    }
  }

  getNewsletterById(id: string) {
    this.newsletterService.getNewsletterById(id).subscribe(newsletter => {
      this.newsLetter = newsletter;
      console.log("UnclassifiedComponent -> getNewsletterById -> newsLetter", this.newsLetter)
    });
  }

  filterNewsItemsOfNewsletter(newsletterId: string) {
    console.log("filtering news items of ", newsletterId);
    this.newsletterService.getNewsItemsOfNewsletter(newsletterId, "2", this.limit, this.page)
      .subscribe(items => {
        //this.newsItems = items;
        this.total = items.total;
        //this.dataSource.data = this.newsItems;
      });
  }

  getAllNewsItems() {
    const sortField: SortField = {
      name: "received_date",
      sortOrder: "-1"
    }
    this.newsletterService.getAllNewsItems(this.limit, this.page, "2", [sortField]).subscribe((items) => {
      if (items) {
        console.log("AllNewsItemsComponent -> getAllNewsItems -> items", items);
        //this.newsItems = items.data;
        this.total = items.total;
        //this.dataSource.data = this.newsItems;
      }
    });
  }

  navigateTo(item) {
    this.router.navigate(['/newsitems/' + item.id]);
  }

}
