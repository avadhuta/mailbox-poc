import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllNewsItemsComponent } from './all-news-items.component';

describe('AllNewsItemsComponent', () => {
  let component: AllNewsItemsComponent;
  let fixture: ComponentFixture<AllNewsItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllNewsItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllNewsItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
