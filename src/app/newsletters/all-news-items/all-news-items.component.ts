
import { Component, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsItem } from 'src/app/entities/newsitem';
import { NewsletterService } from '../../service/newsletter.service';
import { Newsletter } from 'src/app/entities/newsletter';

@Component({
  selector: 'app-all-news-items',
  templateUrl: './all-news-items.component.html',
  styleUrls: ['./all-news-items.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AllNewsItemsComponent implements OnInit {
  allNewItemList: NewsItem[] = [];
  limit: number;
  page: number;
  newsLetter: Newsletter;
  selectedTab: number;

  constructor(
    private route: ActivatedRoute,
    private newsletterService: NewsletterService
  ) {
    this.limit = 20;
    this.page = 0;
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log("AllNewsItemsComponent -> ngOnInit -> params", params)
      if (params['newsletter']) {
        this.getNewsletterById(params['newsletter']);
      } else {
        this.newsLetter = null;
      }
    });
  }

  getNewsletterById(id: string) {
    this.newsletterService.getNewsletterById(id).subscribe(newsletter => {
      this.newsLetter = newsletter;
      console.log("UnclassifiedComponent -> getNewsletterById -> newsLetter", this.newsLetter)
    });
  }

  onSelectedTab(event) {
    console.log("selectedTab event", event);
    this.selectedTab = event.index;
  }

}
