import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewsletterService } from 'src/app/service/newsletter.service';
import { NewsItem } from 'src/app/entities/newsitem';
import { MatDialog } from '@angular/material/dialog';
import {
  ConfirmDialogComponent,
  ConfirmDialogModel,
} from 'src/app/utilities/confirm-dialog/confirm-dialog.component';
import { WorkflowState } from 'src/app/entities/workflow.enum';
import { NewsItemStatus } from 'src/app/entities/news-item-status.enum';
import { Task } from 'src/app/entities/task';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from 'src/app/service/user.service';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-news-item-in-detail',
  templateUrl: './news-item-in-detail.component.html',
  styleUrls: ['./news-item-in-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class NewsItemInDetailComponent implements OnInit {
  newsItem: NewsItem;
  isWorkflowInitiated = false;
  workflowStateId = WorkflowState.Initiation;
  isImpactAssessmentRequired = false;
  classifications = [];
  workflowStates = [];
  tasks: Task[] = [];
  currentUser: User;
  tasksDataSource: MatTableDataSource<Task>;
  displayedColumns: string[] = [
    'user_assigned',
    'start_date',
    'due_date',
    'comments',
    'status',
    'document',
  ];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private newsletterService: NewsletterService,
    public dialog: MatDialog,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      console.log('NewsItemInDetailComponent -> ngOnInit -> params', params);
      this.getNewsItemById(params['id']);
    });
    this.getWorkflowStates();
    this.currentUser = this.userService.getCurrentUser();
    this.tasksDataSource = new MatTableDataSource(this.tasks);
    this.newsletterService.getNewsItemClassifications().subscribe((res) => {
      this.classifications = res;
      console.log(
        'NewsItemInDetailComponent -> checkIfImpactAssessmentRequired -> res',
        res
      );
    });
  }

  getWorkflowStates() {
    this.newsletterService
      .getWorkflowStates()
      .subscribe((states) => (this.workflowStates = states));
  }

  checkIfActionButtonEnabled() {
    if (!this.currentUser) {
      return false;
    }
    console.log('checking action btn: ', this.workflowStateId, this.tasks);
    if (this.newsItem.status_id.id === 1 && this.workflowStateId === 1) {
      return this.currentUser.roles.indexOf(1) !== -1; // only admin can initiate assessment
    }
    if (this.workflowStateId && this.tasks && this.tasks.length > 0) {
      const wf = this.tasks.find((task) => this.workflowStateId === task.workflow_state_id);
      if (!wf) {
        return true;
      }
      return (this.currentUser.id === wf.user_assigned || this.currentUser.roles.indexOf(1) !== -1); // check if admin
    }
    return true;
  }

  getTasks(newsitemId: string) {
    this.newsletterService.getTasksOfNewsItem(newsitemId).subscribe((tasks) => {
      this.tasks = tasks;
      console.log('NewsItemInDetailComponent -> getTasks -> tasks', tasks);
      this.tasksDataSource.data = this.tasks;
    });
  }

  getClassification() {
    if (!this.classifications) {
      return '';
    }
    if (this.newsItem && this.newsItem.classification) {
      const cla = this.newsItem.classification.map((item) => {
        const cItem = this.classifications.find((cl) => cl.id === item);
        return cItem ? cItem.name : '';
      });
      return cla.join();
    }
    return null;
  }

  getNewsItemById(id: string) {
    this.newsletterService.getNewsItemById(id).subscribe((newsItem) => {
      this.newsItem = newsItem;
      if (this.newsItem.workflow_state_id) {
        this.isWorkflowInitiated = true;
        this.workflowStateId = this.newsItem.workflow_state_id;
      }
      if (newsItem.classification.indexOf(1) !== -1) {
        this.isImpactAssessmentRequired = true;
      }
      this.getTasks(this.newsItem.id);
      console.log(
        'NewsItemInDetailComponent -> getNewsItemById -> newsItem',
        newsItem
      );
    });
  }

  onChangeStatus(status) {
    if (this.tasks && this.tasks.length > 0) {
      const activeTasks = this.tasks.filter(task => task.status_id === 1 && task.user_assigned !== this.currentUser.id);
      if (activeTasks.length > 0) {
        activeTasks
        console.log("pending tasks");
        alert("Cannot update status when there are pending tasks, please complete the tasks and try again.");
        return;
      }
    }
    if (status === 'relevant') {
      this.router.navigate(['newsitems/status/change'], {
        queryParams: { newsitem: this.newsItem.id },
      });
      // this.classificationId = Classification.Initiation;
    } else if (status === 'review') {
      this.router.navigate(['newsitems/status/change'], {
        queryParams: {
          newsitem: this.newsItem.id,
          workflow: WorkflowState.Review,
        },
      });
    } else if (status === 'approve') {
      this.router.navigate(['newsitems/status/change'], {
        queryParams: {
          newsitem: this.newsItem.id,
          workflow: WorkflowState.Approval,
        },
      });
    } else if (status === 'implementation') {
      this.router.navigate(['newsitems/status/change'], {
        queryParams: {
          newsitem: this.newsItem.id,
          workflow: WorkflowState.Implementation,
        },
      });
    } else if (status === 'complete') {
      const dialogData = new ConfirmDialogModel(
        'Confirm Action',
        'This will end the assessment workflow, are you sure ?'
      );
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        maxWidth: '400px',
        data: dialogData,
      });
      dialogRef.afterClosed().subscribe((dialogResult) => {
        if (dialogResult) {
          this.closeAssessment();
        }
      });
    } else {
      // this.classificationId = Classification.Initiation;
      const dialogData = new ConfirmDialogModel(
        'Confirm Action',
        'This will change the status of the news item to ' +
        status +
        ', are you sure ?'
      );
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        maxWidth: '400px',
        data: dialogData,
      });

      dialogRef.afterClosed().subscribe((dialogResult) => {
        console.log(
          'NewsItemInDetailComponent -> onChangeStatus -> dialogResult',
          dialogResult
        );
        if (dialogResult) {
          if (status === 'not-relevant') {
            this.newsletterService
              .changeNewsItemStatus(
                this.newsItem.id,
                status === 'relevant' ? '1' : '2'
              )
              .subscribe((res) => {
                console.log(
                  'NewsItemInDetailComponent -> onChangeStatus -> res',
                  res
                );
                this.getNewsItemById(this.newsItem.id);
              });
          }
        }
      });
    }
  }

  closeAssessment() {
    this.newsletterService
      .changeNewsItemWorkflowState(this.newsItem.id, WorkflowState.Closure)
      .subscribe((res) => {
        console.log('RelevantFormComponent -> onSubmit -> res', res);
        //this.snackBar.open('Newsletter marked as relevant', null, { duration: 2000 });
        this.getNewsItemById(this.newsItem.id);
      });
  }

  getWorkflowNameFromId(id: number) {
    switch (id) {
      case WorkflowState.Approval:
        return 'Approval';
      case WorkflowState.Assessment:
        return 'Assessment';
      case WorkflowState.Closure:
        return 'Completed';
      case WorkflowState.Implementation:
        return 'Implementation';
      case WorkflowState.Initiation:
        return 'Initiated';
      case WorkflowState.Review:
        return 'Review';
    }
  }

  onIntitateAssessment() {
    console.log('before classification', this.workflowStateId);
    //this.classificationId = 'submit-for-review';
    this.router.navigate(['/intiate-assessment'], {
      queryParams: { newsitem: this.newsItem.id },
    });
    //console.log('after classification', this.classification);
  }

  onViewAssessment() {
    this.router.navigate(['/assessment-details'], {
      queryParams: { newsitem: this.newsItem.id },
    });
  }

  downLoadFile(file) {
    if (file) {
      window.open(file, '_blank');
    }
  }

  onSubmitForReview() {
    //this.classification = 'approve';
  }

  onApprove() {
    //this.classification = 'complete';
  }

  onComplete() { }

  onNext() {
    console.log('before classification Id', this.workflowStateId);
    this.workflowStateId++;
    console.log('after classification Id', this.workflowStateId);
  }
}
