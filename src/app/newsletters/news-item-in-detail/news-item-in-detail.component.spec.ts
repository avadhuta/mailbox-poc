import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsItemInDetailComponent } from './news-item-in-detail.component';

describe('NewsItemInDetailComponent', () => {
  let component: NewsItemInDetailComponent;
  let fixture: ComponentFixture<NewsItemInDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsItemInDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsItemInDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
