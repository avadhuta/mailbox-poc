import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NewsletterService } from 'src/app/service/newsletter.service';
import { NewsItem } from 'src/app/entities/newsitem';
import { Router, ActivatedRoute } from '@angular/router';
import { Newsletter } from 'src/app/entities/newsletter';
import {
  ConfirmDialogModel,
  ConfirmDialogComponent,
} from 'src/app/utilities/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SortField } from 'src/app/entities/sort-field';
import { NewsitemsDataSource } from 'src/app/utilities/data-table-source';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-unclassified',
  templateUrl: './unclassified.component.html',
  styleUrls: ['./unclassified.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class UnclassifiedComponent implements OnInit {
  @Input() isActive;
  displayedColumns: string[] = ['date', 'title', 'newsletter_id', 'status_id'];
  dataSource: NewsitemsDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  limit: number;
  page: number;
  total: number;
  newsItems: NewsItem[];
  newsLetter: Newsletter;
  newsletterId: string = '';
  sortField: SortField = {
    name: 'received_date',
    sortOrder: '-1',
  };

  constructor(
    private newsletterService: NewsletterService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.limit = 50;
    this.page = 0;
  }

  ngOnInit(): void {
    this.dataSource = new NewsitemsDataSource(this.newsletterService);

    this.route.queryParams.subscribe((params) => {
      console.log('UnclassifiedComponent -> ngOnInit -> params', params);
      if (params['newsletter']) {
        this.filterNewsItemsOfNewsletter(params['newsletter']);
        this.newsletterId = params['newsletter'];
        console.log(this.newsletterId, 'newsletterId');
        this.dataSource.loadNewsitems(
          '3',
          this.newsletterId,
          this.sortField,
          this.page,
          this.limit
        );
      } else {
        this.newsletterId = '';
        this.getAllNewsItems();
        this.dataSource.loadNewsitems(
          '3',
          this.newsletterId,
          this.sortField,
          this.page,
          this.limit
        );
      }
    });
    this.dataSource.newsitemsSubject.subscribe((newsList) => {
      console.log(newsList, 'newlist');
      if (newsList) {
        this.newsItems = newsList;
      }
    });
  }

  onPageChange(event) {
    console.log('UnclassifiedComponent -> onPageChange -> event', event);
    this.limit = event.pageSize;
    this.page = event.pageIndex;
    this.getAllNewsItems();
  }

  onViewNewsletter(event, newsitem) {
    event.stopPropagation();
    this.router.navigate(['/newsitems'], {
      queryParams: { newsletter: newsitem.newsletter_id.id },
    });
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(tap(() => this.loadNewsitemsPage())).subscribe();
  }

  loadNewsitemsPage() {
    this.dataSource.loadNewsitems(
      '3',
      this.newsletterId,
      this.sortField,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
  }

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  //   this.dataSource.sort = this.sort;
  // }

  applyFilter(event: Event) {
    // const filterValue = (event.target as HTMLInputElement).value;
    // this.dataSource.filter = filterValue.trim().toLowerCase();
    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  getNewsletterById(id: string) {
    this.newsletterService.getNewsletterById(id).subscribe((newsletter) => {
      this.newsLetter = newsletter;
      console.log(
        'UnclassifiedComponent -> getNewsletterById -> newsLetter',
        this.newsLetter
      );
    });
  }

  filterNewsItemsOfNewsletter(newsletterId: string) {
    console.log('filtering news items of ', newsletterId);
    this.newsletterService
      .getNewsItemsOfNewsletter(newsletterId, '3', 0, 0)
      .subscribe((items) => {
        this.total = items.total;
        //this.dataSource.data = this.newsItems;
      });
  }

  getAllNewsItems() {
    const sortField: SortField = {
      name: 'received_date',
      sortOrder: '-1',
    };
    this.newsletterService
      .getAllNewsItems(0, 0, '3', [sortField])
      .subscribe((items) => {
        if (items) {
          console.log(
            'AllNewsItemsComponent -> getAllNewsItems -> items',
            items
          );
          this.newsItems = items.data;
          // this.dataSource.data = this.newsItems;
          // this.dataSource._updatePaginator(items.total);
          // this.paginator.pageIndex = this.page;
          // this.paginator.pageSize = this.limit;

          this.total = items.total;
          console.log(
            'UnclassifiedComponent -> getAllNewsItems -> total',
            this.total,
            this.limit,
            this.page
          );
        }
      });
  }

  onNotRelevantClick(event: any, newsItemId: string) {
    console.log(this.newsItems, 'this.newsItems');
    event.stopPropagation();
    const itemToRemove = this.newsItems.findIndex(
      (item) => newsItemId === item.id
    );
    this.newsletterService
      .changeNewsItemStatus(newsItemId, '2')
      .subscribe((res) => {
        console.log('NewsItemInDetailComponent -> onChangeStatus -> res', res);
        this.newsItems.splice(itemToRemove, 1);
        this.loadNewsitemsPage();
      });
    // const dialogData = new ConfirmDialogModel(
    //   'Confirm Action',
    //   'This will change the status of the news item to not relevant, are you sure ?'
    // );
    // const dialogRef = this.dialog.open(ConfirmDialogComponent, {
    //   maxWidth: '400px',
    //   data: dialogData,
    // });

    // dialogRef.afterClosed().subscribe((dialogResult) => {
    //   console.log(
    //     'NewsItemInDetailComponent -> onChangeStatus -> dialogResult',
    //     dialogResult
    //   );
    //   if (dialogResult) {
    //     this.newsletterService
    //       .changeNewsItemStatus(newsItemId, '2')
    //       .subscribe((res) => {
    //         console.log(
    //           'NewsItemInDetailComponent -> onChangeStatus -> res',
    //           res
    //         );
    //         this.newsItems.splice(itemToRemove, 1);
    //         this.loadNewsitemsPage();
    //       });
    //   }
    // });
  }

  onRelevantClick(event: any, newsItemId: string) {
    event.stopPropagation();
    this.router.navigate(['newsitems/status/change'], {
      queryParams: { newsitem: newsItemId },
    });
  }

  navigateTo(item) {
    console.log(item, 'item');
    this.router.navigate(['/newsitems/' + item.id]);
  }
}
