import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MatDialogRef } from '@angular/material/dialog';
import { MyErrorStateMatcher } from 'src/app/utilities/error-state-matcher';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-assessment-dialog',
  templateUrl: './assessment-dialog.component.html',
  styleUrls: ['./assessment-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class AssessmentDialogComponent implements OnInit {
  addAssessmentForm: FormGroup;
  matcher;

  constructor(
    public _bottomSheetRef: MatBottomSheetRef<AssessmentDialogComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data
  ) {
    console.log(data, 'data');
  }

  ngOnInit(): void {
    this.addAssessmentForm = new FormGroup({
      requirement: new FormControl(
        this.data ? this.data.details : '',
        Validators.required
      ),
      impact: new FormControl(
        this.data ? this.data.impact : '',
        Validators.required
      ),
    });
    this.matcher = new MyErrorStateMatcher();
  }

  onSubmit() {
    console.log('requirement values ', this.addAssessmentForm.value);
    this._bottomSheetRef.dismiss(this.addAssessmentForm.value);
  }
}
