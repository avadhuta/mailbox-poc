import { Component, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Newsletter } from '../entities/newsletter';
import { NewsletterService } from '../service/newsletter.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-newsletters',
  templateUrl: './newsletters.component.html',
  styleUrls: ['./newsletters.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class NewslettersComponent implements OnInit {
  newsletters: Newsletter[] = [];
  limit: number;
  page: number;

  displayedColumns: string[] = ['id', 'title', 'received_date', 'total_items', 'active_items', 'status'];
  dataSource: MatTableDataSource<Newsletter>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private newsletterService: NewsletterService, private router: Router) {
    this.limit = 100;
    this.page = 0;
    this.dataSource = new MatTableDataSource(this.newsletters);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit(): void {
    this.getAllNewsletters();
  }

  getAllNewsletters() {
    this.newsletterService.getAllNewsletters(this.limit, this.page).subscribe(items => {
      this.newsletters = this.newsletters.concat(items);
      this.dataSource.data = this.newsletters;
      console.log("NewslettersComponent -> getAllNewsletters -> newsletters", this.newsletters)
    });
  }

  onNewsletterClick(item) {
    console.log("NewslettersComponent -> onNewsletterClick -> item", item);
    this.router.navigate(['/newsitems'], { queryParams: { newsletter: item.id } });
  }

}
