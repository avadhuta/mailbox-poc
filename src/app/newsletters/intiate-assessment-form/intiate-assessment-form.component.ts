import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Location } from '@angular/common';
import { Assessment } from 'src/app/entities/assessment';
import { ActivatedRoute } from '@angular/router';
import { AssessmentService } from 'src/app/service/assessment.service';
import { NewsletterService } from 'src/app/service/newsletter.service';
import { WorkflowState } from 'src/app/entities/workflow.enum';
import { MyErrorStateMatcher } from 'src/app/utilities/error-state-matcher';
import { generateAssessmentNumber } from 'src/app/utilities/assessment-no-generator';
import { Process } from 'src/app/entities/process';
import countriesList from '../../utilities/countries';
import { NewsItem } from 'src/app/entities/newsitem';
import { FileUploadService } from 'src/app/service/file-upload-service';
import { dataURItoBlob, getExactFilePath } from 'src/app/utilities/helper';

@Component({
  selector: 'app-intiate-assessment-form',
  templateUrl: './intiate-assessment-form.component.html',
  styleUrls: ['./intiate-assessment-form.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class IntiateAssessmentFormComponent implements OnInit {
  intiateAssessmentForm: FormGroup;
  newsItemId: string;
  matcher;
  assessmentFormat: any;
  processes: Process[] = [];
  countries = countriesList;
  newsItem: NewsItem;
  fileName: any;
  uploadedUrl: any;
  alertService: any;
  processId = [];
  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private assessmentService: AssessmentService,
    private snackBar: MatSnackBar,
    private newsletterService: NewsletterService,
    private fileUploadService: FileUploadService
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      if (params) {
        this.newsItemId = params['newsitem'];
        this.getNewsItemById(this.newsItemId);
      }
    });
    this.matcher = new MyErrorStateMatcher();
    this.getAssessmentFormat();
    this.getAllProcesses();
    console.log(this.newsItem, 'this.newsItem ');
    this.intiateAssessmentForm = new FormGroup({
      title: new FormControl(
        this.newsItem ? this.newsItem.title : '',
        Validators.required
      ),
      source: new FormControl('', Validators.required),
      awareness_date: new FormControl('', Validators.required),
      assessment_due_date: new FormControl('', Validators.required),
      region_impacted: new FormControl('', Validators.required),
      countries_impacted: new FormControl('', Validators.required),
      function_impacted: new FormControl('', Validators.required),
      process: new FormControl('', Validators.required),
      file_url: new FormControl(''),
    });
  }

  onFileUploadComplete(uploadedUrl) {
    this.uploadedUrl = uploadedUrl;
  }

  onBackClick() {
    this.location.back();
  }

  removeUploadedFile() {
    this.uploadedUrl = '';
  }

  getNewsitemProcess(id) {
    this.newsletterService.getNewsitemProcess(id).subscribe((process) => {
      if (process) {
        this.processId = process;
        const arrayOfProcessId = process.map((item) => item.process_id);
        console.log(arrayOfProcessId, 'arrayOfProcessId');
        this.intiateAssessmentForm.controls['process'].setValue(
          arrayOfProcessId
        );
      }
    });
  }

  getNewsItemById(id: string) {
    this.newsletterService.getNewsItemById(id).subscribe((newsItem) => {
      if (newsItem) {
        console.log(newsItem, 'newsItem');
        this.getNewsitemProcess(newsItem.id);
        this.newsItem = newsItem;
        this.intiateAssessmentForm.controls['title'].setValue(
          this.newsItem.title
        );
        this.intiateAssessmentForm.controls['awareness_date'].setValue(
          this.newsItem['received_date']
        );
        this.intiateAssessmentForm.controls['function_impacted'].setValue(
          this.newsItem['function_impacted']
        );
        this.intiateAssessmentForm.controls['region_impacted'].setValue(
          this.newsItem['region_impacted']
        );
        this.intiateAssessmentForm.controls['countries_impacted'].setValue(
          this.newsItem['country_impacted']
        );
        console.log(
          'NewsItemInDetailComponent -> getNewsItemById -> newsItem',
          this.newsItem
        );
      }
    });
  }

  getAssessmentFormat() {
    this.assessmentService.getAssessmentNoFormat().subscribe((format) => {
      this.assessmentFormat = format && format.length > 0 ? format[0] : null;
      console.log(
        'IntiateAssessmentFormComponent -> getAssessmentFormat -> assessmentFormat',
        this.assessmentFormat,
        format
      );
    });
  }

  onDownloadFile(url: string) {
    this.fileUploadService.downloadFile(url).subscribe((blob) => {
      const fileURL = window.URL.createObjectURL(blob);
      window.open(url, '_blank');
    });
  }

  getAllProcesses() {
    this.assessmentService
      .getAllProcesses()
      .subscribe((process) => (this.processes = process));
  }

  addAssessmentProcess(assessmentId: string, processes: string[]) {
    this.assessmentService
      .createAssessmentProcess(assessmentId, processes)
      .subscribe(() => console.log('added process'));
  }

  onSubmit() {
    // create assessment
    this.intiateAssessmentForm.controls['file_url'].setValue(this.uploadedUrl);

    let value = this.intiateAssessmentForm.value;
    console.log(value, 'value');
    const processes = value.process;
    delete value['process'];
    value['newsitem_id'] = this.newsItemId;
    value['assessment_no'] = generateAssessmentNumber(
      this.assessmentFormat ? this.assessmentFormat.format : null
    );
    value['file_url'] = this.uploadedUrl;
    console.log(
      'IntiateAssessmentFormComponent -> onSubmit -> creating assessment',
      value
    );
    this.assessmentService.createAssessment(value).subscribe((res) => {
      console.log('IntiateAssessmentFormComponent -> onSubmit -> res', res);
      this.addAssessmentProcess(res['id'], processes);
      this.assessmentService
        .changeNewsItemWorkflowState(
          value['newsitem_id'],
          WorkflowState.Assessment
        )
        .subscribe((classChanged) => {
          console.log(
            'IntiateAssessmentFormComponent -> onSubmit -> classChanged',
            classChanged
          );
          this.snackBar.open('Sucessfully initiated assessment', null, {
            duration: 2000,
          });
          this.location.back();
        });
    });
  }
}
