import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntiateAssessmentFormComponent } from './intiate-assessment-form.component';

describe('IntiateAssessmentFormComponent', () => {
  let component: IntiateAssessmentFormComponent;
  let fixture: ComponentFixture<IntiateAssessmentFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntiateAssessmentFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntiateAssessmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
