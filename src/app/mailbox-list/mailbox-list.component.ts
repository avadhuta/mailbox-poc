import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { Message } from '../model/message';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mailbox-list',
  templateUrl: './mailbox-list.component.html',
  styleUrls: ['./mailbox-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MailboxListComponent implements OnInit {
  @Input() mailList: Message[] = [];
  @Output() selectedMessageChangeO = new EventEmitter();
  selectedIndex = 0;
  searchString = '';
  constructor(private router: Router) {}

  ngOnInit(): void {}

  onSelection(selectedMessage: Message, index) {
    this.selectedIndex = index;
    this.selectedMessageChangeO.emit(selectedMessage);
  }
}
