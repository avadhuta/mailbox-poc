import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailboxContentComponent } from './mailbox-content.component';

describe('MailboxContentComponent', () => {
  let component: MailboxContentComponent;
  let fixture: ComponentFixture<MailboxContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailboxContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailboxContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
