import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { Message } from '../model/message';
import { MessageService } from '../service/message.service';
import { MatDialog } from '@angular/material/dialog';
import { ReplyMailDialogComponent } from '../reply-mail-dialog/reply-mail-dialog.component';
import { ForwardMailDialogComponent } from '../forward-mail-dialog/forward-mail-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NewsletterService } from '../service/newsletter.service';
import { Router } from '@angular/router';
import { InboxService } from '../service/inbox.service';
import { AlertService } from '../shared/alert/alert.service';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../utilities/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-mailbox-content',
  templateUrl: './mailbox-content.component.html',
  styleUrls: ['./mailbox-content.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class MailboxContentComponent implements OnInit {
  @Input() selectedMessage: Message;
  constructor(
    private newsletterService: NewsletterService, private alertService: AlertService,
    public dialog: MatDialog, private inboxService: InboxService,
    public snackBar: MatSnackBar, private router: Router
  ) { }

  ngOnInit(): void { }

  ngOnChanges(): void {
    console.log('selected message', this.selectedMessage);
  }

  openReplyMailDialog() {
    const dialogRef = this.dialog.open(ReplyMailDialogComponent, {
      height: '400px',
      width: '600px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      // console.log(`Dialog result: ${result}`);
    });
  }

  openForwardMailDialog() {
    const dialogRef = this.dialog.open(ForwardMailDialogComponent, {
      height: '400px',
      width: '600px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      // console.log(`Dialog result: ${result}`);
    });
  }

  openSnackBar() {
    this.snackBar.open('Feature yet to be implemented', null, {
      duration: 2000,
    });
  }

  onArchiveMailClick() {
    const dialogData = new ConfirmDialogModel('Confirm Action', 'This will archive the mail in the connected mailbox, are you sure ?');
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      console.log('NewsItemInDetailComponent -> onChangeStatus -> dialogResult', dialogResult);
      if (dialogResult) {
        this.inboxService.archiveMail("" + this.selectedMessage.id).subscribe(() => {
          this.alertService.info("Mail archived successfully in the connected mailbox.");
        })
      }
    });
  }

  onViewNewsletter() {
    this.newsletterService.getNewsletterByMailId("" + this.selectedMessage.id).subscribe(newsletter => {
      console.log("MailboxContentComponent -> onViewNewsletter -> newsletter", newsletter);
      this.router.navigate(['/newsitems'], { queryParams: { newsletter: newsletter.id } })
    })
  }
}
