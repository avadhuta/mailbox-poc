export interface Requirement {
    id: string;
    details: string;
    impact: string;
    assessment_id: string;
}