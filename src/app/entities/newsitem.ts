export interface NewsItem {
  classification: any;
  description: string;
  detail_link: string;
  detail_link_name: string;
  newsletter_id: number;
  id: string;
  status_id: {
    id: number;
    status: string;
  };
  title: string;
  workflow_state_id: number;
}
