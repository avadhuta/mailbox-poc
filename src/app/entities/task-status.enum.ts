export enum TaskStatus {
    Active = 1,
    Completed
}