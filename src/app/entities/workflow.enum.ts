export enum WorkflowState {
  Initiation = 1,
  Assessment,
  Review,
  Approval,
  Implementation,
  Closure
}
