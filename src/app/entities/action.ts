export interface Action {
    id: string;
    description: string;
    due_date: Date;
    evidence_file_url: string;
    assessment_id: string;
    requirement_id: string;
    status_id: number;
    assigned_user_id: string;
}