export interface Newsletter {
    id: number;
    title: string;
    received_date: Date;
    total_items: number;
    active_items: number;
    is_closed: boolean;
    mail_id: string;
}