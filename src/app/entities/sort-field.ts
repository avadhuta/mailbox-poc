export interface SortField {
    name: string;
    sortOrder: string; // asc or desc
}
