export enum NewsItemStatus {
    Relevant = 1,
    NotRelevant,
    Unclassified
}