export interface Task {
  id: string;
  start_date: Date;
  due_date: Date;
  status_id: number;
  comments: string;
  created_date: Date;
  user_assigned: string;
  newsitem_id: string;
  workflow_state_id: number;
  document_url: string;
}
