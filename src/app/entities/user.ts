export interface UserProfile {
  id: number;
  email: string;
  name: string;
  avatar_url: string;
  roles: any[];
}
