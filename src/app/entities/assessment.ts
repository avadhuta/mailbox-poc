
export interface Assessment {
  id: string,
  title: string,
  awareness_date: Date,
  assessment_due_date: Date,
  region_impacted: string,
  countries_impacted: string,
  function_impacted: string,
  source: string,
  file_url: string,
  newsitem_id: string
  assessment_no: string;
  process: any[];
}
