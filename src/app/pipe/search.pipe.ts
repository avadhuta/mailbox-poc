import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'seacrhFilter',
    pure: true
})
export class SearchFilterPipe implements PipeTransform {
    transform(items: any, searchValue: string): any {
        if (!Array.isArray(items)) {
            return items;
        }
        const lowerCasedValue = searchValue.toLowerCase().trim();
        const filteredData = items.filter((item: any) => {
            return Object.keys(item).some((key) =>
                item[key] !== null
                    ? item[key].toString().toLowerCase().includes(lowerCasedValue)
                    : null
            );
        });
        return filteredData;
    }
}