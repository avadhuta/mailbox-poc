export interface User {
  id: string;
  email: string;
  token: string;
  name: string;
  roles: any[];
}
