export class Message {
  id: number;
  date: string;
  subject: string;
  content: string;
  attachment: string;
  from_address: string;
  to_address: string;
  received_date: string;
  html: string;
  from_name: string;
  to_name: string;
}
