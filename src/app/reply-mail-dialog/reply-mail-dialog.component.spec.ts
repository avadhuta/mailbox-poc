import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplyMailDialogComponent } from './reply-mail-dialog.component';

describe('ReplyMailDialogComponent', () => {
  let component: ReplyMailDialogComponent;
  let fixture: ComponentFixture<ReplyMailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplyMailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplyMailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
