import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-reply-mail-dialog',
  templateUrl: './reply-mail-dialog.component.html',
  styleUrls: ['./reply-mail-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class ReplyMailDialogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
