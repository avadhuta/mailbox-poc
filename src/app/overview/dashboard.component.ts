import { AddActionDialogComponent } from './../newsletters/add-action-dialog/add-action-dialog.component';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { StatsService } from '../service/stats.service';
import { NewsletterService } from '../service/newsletter.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class DashboardComponent implements OnInit {
  newsletterCount = 0;
  openItemCount = 0;
  closedItemsCount = 0;
  relevantCount = 0;
  notRelevantCount = 0;
  impactClassCount = 0;
  newsletterClassCount = 0;
  complianceClassCount = 0;
  overdueTaskCount = 0;
  filter = 'country';
  classificationId = '1';
  classifications = [];
  filters = ['all', 'process', 'country'];

  multi = [];
  view: any[] = [800, 500];

  // options
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = false;
  showLegend: boolean = true;
  legendPosition: string = 'below';
  schemeType: string = 'linear';

  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'No of News Items';
  yAxisLabel: string = 'Country';
  timeline: boolean = true;

  colorScheme = {
    //domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
    domain: ['#efefef', '#7aa3e5'],
  };

  constructor(
    private statsService: StatsService,
    private newsletterService: NewsletterService,
    private router: Router
  ) {
    //Object.assign(this, { multi });
  }

  ngOnInit(): void {
    this.getNewsletterCount();
    this.getOpenNewsItemsCount();
    this.getClosedNewsItemsCount();
    this.getRelevantNewsItemsCount();
    this.getNotRelevantItemsCount();
    this.getImpactCount();
    this.getComplianceCount();
    this.getNewsletterClassCount();
    this.getGraphData();
    this.getClassifications();
    this.getOverdueTasksCount();
  }

  getClassifications() {
    this.newsletterService
      .getNewsItemClassifications()
      .subscribe((classes) => (this.classifications = classes));
  }

  getOverdueTasksCount() {
    this.statsService
      .getOverdueTasksCount()
      .subscribe((count) => (this.overdueTaskCount = count));
  }
  onChangeClassifiction() {
    this.getGraphData();
  }
  onChangeFilter() {
    this.getGraphData();
  }

  getGraphData() {
    this.statsService
      .getGraphData(this.classificationId, this.filter)
      .subscribe((res) => {
        console.log('DashboardComponent -> getGraphData -> res', res);
        let data = [];
        if (this.filter === 'all') {
          this.yAxisLabel = "Date";
          const item = { name: 'all', series: [] };
          item.series = res.map((row) => {
            return {
              name: new Date(row.received_date).toDateString(),
              value: row.newsitems,
            };
          });
          data.push(item);
        } else if (this.filter === 'country') {
          this.yAxisLabel = "Country";
          const grouped = this.groupByCountry(res);
          console.log('DashboardComponent -> getGraphData -> grouped', grouped);
          Object.keys(grouped).map((key) => {
            const item = { name: key, series: [] };
            item.series = grouped[key].map((row) => {
              return {
                name: new Date(row.received_date).toDateString(),
                value: row.newsitems,
              };
            });
            data.push(item);
          });
        } else if (this.filter === 'process') {
          this.yAxisLabel = "Process";
          const grouped = this.groupByProcess(res);
          console.log('DashboardComponent -> getGraphData -> grouped', grouped);
          Object.keys(grouped).map((key) => {
            const item = { name: 'Process ' + key, series: [] };
            item.series = grouped[key].map((row) => {
              return {
                name: new Date(row.received_date).toDateString(),
                value: row.newsitems,
              };
            });
            data.push(item);
          });
        }
        console.log('graph data', data);
        this.multi = data;
      });
  }

  groupByCountry(data: any) {
    return data.reduce(
      (objectsByKeyValue: any, obj: any) => ({
        ...objectsByKeyValue,
        [obj['country_impacted']]: (
          objectsByKeyValue[obj['country_impacted']] || []
        ).concat(obj),
      }),
      {}
    );
  }

  groupByProcess(data: any) {
    return data.reduce(
      (objectsByKeyValue: any, obj: any) => ({
        ...objectsByKeyValue,
        [obj['process_id']]: (
          objectsByKeyValue[obj['process_id']] || []
        ).concat(obj),
      }),
      {}
    );
  }

  getNewsletterCount() {
    this.statsService.getNewsletterCount().subscribe((count) => {
      console.log('DashboardComponent -> getNewsletterCount -> count', count);
      this.newsletterCount = count;
    });
  }

  getOpenNewsItemsCount() {
    this.statsService.getOpenNewsItemsCount().subscribe((count) => {
      console.log('DashboardComponent -> getNewsletterCount -> count', count);
      this.openItemCount = count;
    });
  }

  getClosedNewsItemsCount() {
    this.statsService.getClosedNewsItemsCount().subscribe((count) => {
      console.log('DashboardComponent -> getNewsletterCount -> count', count);
      this.closedItemsCount = count;
    });
  }

  getRelevantNewsItemsCount() {
    this.statsService.getRelevantItemsCount().subscribe((count) => {
      console.log('DashboardComponent -> getNewsletterCount -> count', count);
      this.relevantCount = count;
    });
  }

  getNotRelevantItemsCount() {
    this.statsService.getNotRelevantItemsCount().subscribe((count) => {
      console.log('DashboardComponent -> getNewsletterCount -> count', count);
      this.notRelevantCount = count;
    });
  }

  getImpactCount() {
    this.statsService.getClassificationCount('1').subscribe((count) => {
      console.log('DashboardComponent -> getNewsletterCount -> count', count);
      this.impactClassCount = count;
    });
  }

  getNewsletterClassCount() {
    this.statsService.getClassificationCount('3').subscribe((count) => {
      console.log('DashboardComponent -> getNewsletterCount -> count', count);
      this.newsletterClassCount = count;
    });
  }

  getComplianceCount() {
    this.statsService.getClassificationCount('2').subscribe((count) => {
      console.log('DashboardComponent -> getNewsletterCount -> count', count);
      this.complianceClassCount = count;
    });
  }

  onGraphSelect(event) {
    console.log('event data: ', event);
    const receivedDate = new Date(event.name);
    if (event.series.indexOf('all') !== -1) {
      this.router.navigate(['/newsitems/graph/view'], {
        queryParams: { receivedDate },
      });
    } else if (event.series.indexOf('Process') !== -1) {
      const processId = event.series.split(' ')[1];
      console.log(
        'DashboardComponent -> onGraphSelect -> processId',
        processId
      );
      this.router.navigate(['/newsitems/graph/view'], {
        queryParams: { processId, receivedDate },
      });
    } else {
      this.router.navigate(['/newsitems/graph/view'], {
        queryParams: { country: event.series, receivedDate },
      });
    }
  }
}
