import { InjectionToken } from '@angular/core';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptorService } from './loader-interceptor';
import { AuthInterceptor } from 'src/app/core/interceptor/auth-interceptor';
import { ErrorHandlerService } from './error-handler-client';

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptorService, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];
