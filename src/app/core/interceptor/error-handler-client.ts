import { Injectable, ErrorHandler, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/shared/alert/alert.service';

@Injectable({
    providedIn: 'root'
})
export class ErrorHandlerService implements ErrorHandler {
    private router: Router;
    constructor(private alertService: AlertService, private injector: Injector) { }

    handleError(error: any) {
        this.router = this.injector.get(Router);
        if (error instanceof HttpErrorResponse) {
            // Backend returns unsuccessful response codes such as 404, 500 etc.
            if (error.status === 401) {
                this.router.navigate(['login']);
                return;
            }
            this.alertService.error(error.message);
            //  console.log(error.message);

        } else {
            // A client - side or network error occurred.
            this.alertService.error(error.message);
            // console.log(error.message, 'client-error');
            setTimeout(() => {
                this.router.navigate(['technicalError']);
            }, 3000);
        }
    }

}
