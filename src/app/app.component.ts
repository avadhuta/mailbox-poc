import { User } from './model/user';
import { UserService } from './service/user.service';
import {
  Component,
  OnDestroy,
  ChangeDetectorRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import {
  ActivatedRoute,
  Router,
  NavigationStart,
  NavigationEnd,
} from '@angular/router';
import { PlatformLocation, Location } from '@angular/common';
import { HostListener } from '@angular/core';
import { SidenavService } from './service/sidenav.servcie';
import { MatSidenav } from '@angular/material/sidenav';
import { LoaderService } from './shared/loader/loader-service';
import { AuthTokenService } from './service/auth-token.service';
import { generateAssessmentNumber } from './utilities/assessment-no-generator';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  title = 'mailbox-poc';
  hideTopbar = false;
  currentUrl: string;
  showFiller = false;
  isLoading = false;
  enableBackButton = false;
  currentUser: User;
  reDirectUrl = '';
  @ViewChild('sidenav', { static: true }) public sidenav: MatSidenav;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private location: Location,
    private sidenavService: SidenavService,
    private loaderService: LoaderService,
    private tokenService: AuthTokenService
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart) {
        this.reDirectUrl = event.url;
      }
      if (event instanceof NavigationEnd) {
        this.currentUrl = this.router.url;
        this.title = this.getTitleByURL(this.currentUrl);
        this.enableBackButton = this.checkIfBackButtonNeeded(this.currentUrl);
        this.hideTopbar =
          this.router.url === '/login' || this.router.url === '/register'
            ? true
            : false;
      }
    });
    this.loaderService.isLoading().subscribe((loading) => {
      this.isLoading = loading;
    });

    setTimeout(() => {
      this.checkCurrentUser();
    }, 500);
  }

  onBackClick() {
    this.location.back();
  }

  goHome() {
    //console.log(this.router.url, this.reDirectUrl, 'this.reDirectUrl');
    let isQueryParams: boolean;
    isQueryParams = decodeURI(this.reDirectUrl).includes('?');
    if (isQueryParams) {
      const reDirectUrl: any = decodeURI(this.reDirectUrl).split('?');
      const queryParams = reDirectUrl[1];
      const param = queryParams.split('&');
      let pair = null;
      const queryParamsdata = {};
      param.forEach((d: any) => {
        pair = d.split('=');
        queryParamsdata[`${pair[0]}`] = pair[1];
      });
      console.log(queryParamsdata, 'app-route-data');
      this.router.navigate([reDirectUrl[0]], {
        queryParams: queryParamsdata,
      });
    } else {
      const reDirectUrl: any = decodeURI(this.reDirectUrl);
      console.log(reDirectUrl, 'reDirectUrl');
      this.router.navigate([reDirectUrl]);
    }
  }

  getTitleByURL(url) {
    if (url.indexOf('/newsitems/') !== -1) {
      return 'NEWS ITEM DETAILS';
    }
    if (url.includes('/newsitems')) {
      return 'NEWS ITEMS';
    }
    if (url.includes('/assessment-details')) {
      return 'ASSESSMENT DETAILS';
    }
    if (url.includes('/actions')) {
      return 'ASSESSMENT ACTIONS';
    }
    switch (url) {
      // case "/newsitems": return "NEWS ITEMS";
      case '/newsletters':
        return 'NEWS LETTERS';
      case '/inbox':
        return 'INBOX';
      case '/my-tasks':
        return 'MY TASKS';
      case '/users':
        return 'USERS';
      case '/dashboard':
        return 'DASHBOARD';
      case '/settings':
        return 'SETTINGS';
      case '/configuration':
        return 'CONFIGURATION';
      case '/reports':
        return 'REPORTS';
      default:
        return 'Pinnapt';
    }
  }

  checkIfBackButtonNeeded(url) {
    if (url.indexOf('/newsitems/') !== -1) {
      return true;
    }
    if (url.includes('/newsitems')) {
      return true;
    }
    if (url.includes('/assessment-details')) {
      return true;
    }
    if (url.includes('/actions')) {
      return true;
    }
    return false;
  }

  ngOnInit() {
    this.sidenavService.setSidenav(this.sidenav);
    this.getCurrentUser();
    this.currentUser = this.userService.getCurrentUser();
  }

  toggle() {
    this.sidenavService.toggle();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  getCurrentUser() {
    this.userService.currentUserChanged.subscribe((user: User) => {
      if (user) {
        this.currentUser = user;
        console.log('current user', this.currentUser);
      }
    });
  }

  logOut() {
    this.sidenavService.close();
    setTimeout(() => {
      localStorage.clear();
      this.tokenService.clear();
      this.router.navigate(['/login']);
    }, 500);
  }

  checkCurrentUser() {
    const token = this.tokenService.getToken();
    if (token) {
      if (localStorage && localStorage.getItem('user')) {
        const user = JSON.parse(localStorage.getItem('user'));
        console.log(user, 'user');
        this.userService.setCurrentUser(user);
        // console.log('check user url', this.currentUrl);
        // this.router.navigate([this.currentUrl]);
        this.getCurrentUser();
      }
      this.goHome();
    } else {
      this.router.navigate(['/dashboard']);
    }
  }
}
