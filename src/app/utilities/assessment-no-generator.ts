export const generateAssessmentNumber = (config?: string) => {
    //const config = `AB-{day}-{year}-{time}-{month}-{digits(14)}-{string(5)}`
    if (!config) {
        config = "AS-{string(6)}"
    }
    let parsed = config.match(/{.*?}/g);
    let result = config;

    parsed.map(item => {
        switch (item) {
            case "{day}": result = result.replace("{day}", "" + new Date().getDay());
            case "{month}": result = result.replace("{month}", "" + (new Date().getMonth() + 1));
            case "{year}": result = result.replace("{year}", "" + new Date().getFullYear());
            case "{time}": result = result.replace("{time}", "" + new Date().getHours() + "-" + new Date().getMinutes());
        }
        if (item.indexOf("digits") !== -1) {
            result = result.replace(item, handleDigits(item));
        }

        if (item.indexOf("string") !== -1) {
            result = result.replace(item, handleString(item));
        }
    })

    console.log("generateAssessmentNumber -> parsed", parsed);
    console.log("generateAssessmentNumber -> result", result)
    return result;
}

const handleDigits = (data: string) => {
    let value = data.substring(data.indexOf("(") + 1, data.indexOf(")"));
    console.log("handleDigits -> value", value);
    return randomDigits(+value);
}

const handleString = (data: string) => {
    let value = data.substring(data.indexOf("(") + 1, data.indexOf(")"));
    console.log("handleString -> value", value);
    return randomString(+value);
}

const randomDigits = (length) => {
    var result = '';
    var characters = '0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

const randomString = (length) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}