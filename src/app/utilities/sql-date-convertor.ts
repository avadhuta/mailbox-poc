function pad2(number) {
    var str = '' + number;
    while (str.length < 2) {
        str = '0' + str;
    }
    return str;
}

function getDateInSqlFormat(date) {
    var dtstring = date.getFullYear()
        + '-' + pad2(date.getMonth() + 1)
        + '-' + pad2(date.getDate())
        + ' ' + pad2(date.getHours())
        + ':' + pad2(date.getMinutes())
        + ':' + pad2(date.getSeconds());
    return dtstring;
}

export default getDateInSqlFormat;