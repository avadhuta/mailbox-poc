import { Observable, of, throwError } from 'rxjs';

export const handleErrorObservable = function <T>(
  operation = 'operation',
  result?: T
) {
  return (error: any): Observable<T> => {
    // TODO: send the error to remote logging infrastructure
    console.error(error, 'handleerrorobsrbale'); // log to console instead

    // TODO: better job of transforming error for user consumption
    console.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return throwError(error);
  };
};

export function getExactFilePath(str) {
  const pos = str.lastIndexOf(str.charAt(str.indexOf(':') + 1));
  return str.substring(pos + 1);
}

export const getMimeTypeFOrDataUrI = function (type) {
  if (type === 'pdf') {
    return 'application/pdf';
  }
  if (type === 'xlsx') {
    return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet/xlsx';
  }
  if (type === 'txt') {
    return 'text/txt';
  }
  if (type === 'csv') {
    return 'text/csv';
  }

  if (type === 'doc' || type === 'dot' || type === 'docx') {
    return `application/msword/${type}`;
  }
  if (type === 'jpg' || type === 'png' || type === 'jpeg') {
    return `image/${type}`;
  }

  if (type === 'xls' || type === 'xlt' || type === 'xla') {
    return `application/vnd.ms-excel/${type}`;
  }
};

export function dataURItoBlob(dataURI, type) {
  const byteString = atob(dataURI);
  const arrayBuffer = new ArrayBuffer(byteString.length);
  const int8Array = new Uint8Array(arrayBuffer);
  for (let i = 0; i < byteString.length; i++) {
    int8Array[i] = byteString.charCodeAt(i);
  }
  const mimiType = getMimeTypeFOrDataUrI(type);
  console.log(mimiType, 'mimtype');
  const blob = new Blob([arrayBuffer], { type: mimiType });
  return blob;
}
