import { DataSource } from '@angular/cdk/table';
import { NewsItem } from '../entities/newsitem';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { CollectionViewer } from '@angular/cdk/collections';
import { catchError, finalize } from 'rxjs/operators';
import { ReportService } from '../service/report.service';

export class ReportDataSource implements DataSource<NewsItem> {
  public newsitemsSubject = new BehaviorSubject<NewsItem[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private reportService: ReportService) { }

  connect(collectionViewer: CollectionViewer): Observable<NewsItem[]> {
    return this.newsitemsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.newsitemsSubject.complete();
    this.loadingSubject.complete();
  }

  loadNewsitems(
    pageIndex = 0,
    pageSize = 3,
    startDate,
    endDate
  ) {
    console.log("ReportDataSource -> constructor -> pageIndex", pageIndex, pageSize);
    this.loadingSubject.next(true);
    this.reportService.getAuditTrailReport(startDate, endDate, pageSize, pageIndex)
      .pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(report => {
        console.log("ReportsComponent -> getAuditTrailReport -> res", report);
        this.newsitemsSubject.next(report.data)
      })
  }
}
