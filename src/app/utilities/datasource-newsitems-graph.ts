import { DataSource } from '@angular/cdk/table';
import { NewsItem } from '../entities/newsitem';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { NewsletterService } from '../service/newsletter.service';
import { CollectionViewer } from '@angular/cdk/collections';
import { catchError, finalize } from 'rxjs/operators';

export class GraphNewsitemsDataSource implements DataSource<NewsItem> {
    public newsitemsSubject = new BehaviorSubject<NewsItem[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private newsletterService: NewsletterService) { }

    connect(collectionViewer: CollectionViewer): Observable<NewsItem[]> {
        return this.newsitemsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.newsitemsSubject.complete();
        this.loadingSubject.complete();
    }

    loadNewsitems(
        processId: string,
        country: string,
        receivedDate: Date,
        pageIndex = 0,
        pageSize = 1000
    ) {
        this.loadingSubject.next(true);
        this.newsletterService.getNewsItemsForGraphCondition(country, processId, receivedDate, pageSize, pageIndex)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            ).subscribe((newsitems) => {
                console.log(
                    'NewsitemsDataSource -> loadNewsitems -> newsitems',
                    newsitems
                );

                this.newsitemsSubject.next(newsitems.data);
            });
    }
}
