import { DataSource } from '@angular/cdk/table';
import { NewsItem } from '../entities/newsitem';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { NewsletterService } from '../service/newsletter.service';
import { CollectionViewer } from '@angular/cdk/collections';
import { catchError, finalize } from 'rxjs/operators';

export class NewsitemsDataSource implements DataSource<NewsItem> {
  public newsitemsSubject = new BehaviorSubject<NewsItem[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private newsletterService: NewsletterService) {}

  connect(collectionViewer: CollectionViewer): Observable<NewsItem[]> {
    return this.newsitemsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.newsitemsSubject.complete();
    this.loadingSubject.complete();
  }

  loadNewsitems(
    statusId: string,
    newsletterId: string,
    sortField,
    pageIndex = 0,
    pageSize = 3,
    classification = null
  ) {
    this.loadingSubject.next(true);

    if (newsletterId) {
      this.newsletterService
        .getNewsItemsOfNewsletter(
          newsletterId,
          statusId,
          pageSize,
          pageIndex,
          [sortField],
          classification
        )
        .pipe(
          catchError(() => of([])),
          finalize(() => this.loadingSubject.next(false))
        )
        .subscribe((newsitems) => {
          console.log(
            'NewsitemsDataSource -> loadNewsitems -> newsitems',
            newsitems
          );

          this.newsitemsSubject.next(newsitems.data);
        });
    } else {
      this.newsletterService
        .getAllNewsItems(
          pageSize,
          pageIndex,
          statusId,
          [sortField],
          classification
        )
        .pipe(
          catchError(() => of([])),
          finalize(() => this.loadingSubject.next(false))
        )
        .subscribe((newsitems) => this.newsitemsSubject.next(newsitems.data));
    }
  }
}
