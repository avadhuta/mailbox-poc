import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForwardMailDialogComponent } from './forward-mail-dialog.component';

describe('ForwardMailDialogComponent', () => {
  let component: ForwardMailDialogComponent;
  let fixture: ComponentFixture<ForwardMailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForwardMailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForwardMailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
