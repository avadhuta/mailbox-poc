import { Component, OnInit } from '@angular/core';
import { AssessmentService } from '../service/assessment.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  format: string;
  assessmentFormat: any;
  constructor(private assessmentService: AssessmentService) { }

  ngOnInit(): void {
    this.assessmentService.getAssessmentNoFormat().subscribe(res => {
      console.log("SettingsComponent -> ngOnInit -> res", res);

      this.assessmentFormat = res && res.length > 0 ? res[0] : null;
      this.format = this.assessmentFormat.format;
    });
  }

  onSetFormat() {
    if (this.assessmentFormat) {
      this.assessmentService.updateAssessmentNoFormat(this.assessmentFormat.id, this.format).subscribe(res => {
        console.log("SettingsComponent -> onSetFormat -> res", res);
        this.assessmentFormat = res;
        this.format = this.assessmentFormat.format;
      })
    } else {
      this.assessmentService.createAssessmentNoFormat(this.format).subscribe(res => {
        this.assessmentFormat = res;
        this.format = this.assessmentFormat.format;
        console.log("SettingsComponent -> onSetFormat -> res -> create", res);
      })
    }
  }

}
