import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Task } from '../entities/task';
import { NewsletterService } from '../service/newsletter.service';
import { UserService } from '../service/user.service';
import { TaskStatus } from '../entities/task-status.enum';
import { User } from '../model/user';
import {
  ConfirmDialogModel,
  ConfirmDialogComponent,
} from '../utilities/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-tasks',
  templateUrl: './my-tasks.component.html',
  styleUrls: ['./my-tasks.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MyTasksComponent implements OnInit {
  tasks: Task[] = [];
  currentUser: User;
  tasksDataSource: MatTableDataSource<Task>;
  selectedTask = 'All';
  displayedColumns: string[] = [
    'newsitem_id',
    'comments',
    'start_date',
    'due_date',
    'actions',
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private newsletterService: NewsletterService, private router: Router,
    private userService: UserService,
    public dialog: MatDialog
  ) {
    this.tasksDataSource = new MatTableDataSource(this.tasks);
  }

  ngAfterViewInit() {
    this.tasksDataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tasksDataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit(): void {
    this.currentUser = this.userService.getCurrentUser();
    this.getTasks(this.currentUser.id);
  }

  navigateToNewsitem(task: any) {
    this.router.navigate(["/newsitems/" + task.newsitem_id.id]);
  }

  completeTask(taskId: string) {
    const dialogData = new ConfirmDialogModel(
      'Confirm Action',
      'This will complete the task, are you sure ?'
    );
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      console.log(
        'NewsItemInDetailComponent -> onChangeStatus -> dialogResult',
        dialogResult
      );
      if (dialogResult) {
        this.newsletterService.completeTask(taskId).subscribe((res) => {
          console.log('MyTasksComponent -> completeTask -> res', res);
          this.getTasks(this.currentUser.id);
        });
      }
    });
  }

  isTaskComplete(statusId: number) {
    return statusId ? statusId === TaskStatus.Completed : false;
  }

  checkStatus(mode) {
    this.selectedTask = mode;
    this.filterTaskData(mode);
  }

  filterTaskData(mode) {
    const cloneMyTask = [...this.tasks];
    if (mode === 'All') {
      this.tasksDataSource = new MatTableDataSource(cloneMyTask);
    } else if (mode === 'Active') {
      const filtredActiveData = cloneMyTask.filter(
        (item) => item.status_id === 1
      );
      this.tasksDataSource = new MatTableDataSource(filtredActiveData);
    } else if (mode === 'Completed') {
      const filtredCompletedData = cloneMyTask.filter(
        (item) => item.status_id === 2
      );
      this.tasksDataSource = new MatTableDataSource(filtredCompletedData);
    }
  }

  getTasks(userId: string) {
    this.newsletterService.getTasksOfUser(userId).subscribe((tasks) => {
      this.tasks = tasks;
      console.log(this.tasks, 'tasks');
      this.tasksDataSource.data = this.tasks;
    });
  }
}
