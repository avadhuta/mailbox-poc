import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UserService } from '../service/user.service';
import { User } from '../model/user';
import { Role } from '../entities/role';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class UsersComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  users: User[] = [];
  roles: Role[] = [];
  displayedColumns: string[] = ['name', 'email', 'roles'];
  dataSource: MatTableDataSource<any>;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getAllUsers().subscribe((users) => {
      if (users) {
        this.users = users;
        this.dataSource = new MatTableDataSource(users);
      }
    });
    this.userService.getAllRoles().subscribe((roles) => (this.roles = roles));
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getRoleName(id: number) {
    if (!this.roles) return '';
    const role = this.roles.find((role) => role.id === id);
    if (role) {
      return role.role_name + ' ';
    } else {
      return '';
    }
  }
}
