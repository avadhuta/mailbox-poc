import { MessageService } from '../service/message.service';
import { Component, OnInit, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Message } from '../model/message';
import { UserService } from '../service/user.service';
import { User } from '../model/user';
import { InboxService } from '../service/inbox.service';
import { AlertService } from '../shared/alert/alert.service';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class InboxComponent implements OnInit {
  mailList: Message[] = [];
  selectedMessage: Message;
  activeMailBox: string;
  currentUser: User;
  currentMailBoxState: string;
  events: string[] = [];
  opened: boolean;
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private messageService: MessageService,
    private inboxService: InboxService, private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.getInboxMessages();
  }

  getInboxMessages() {
    this.inboxService.getAllInboxMail().subscribe((mailList) => {
      if (mailList) {
        console.log(mailList, 'mailList');
        this.mailList = mailList;
        this.mailList.sort((a, b) => {
          return (new Date(b.date) as any) - (new Date(a.date) as any);
        });
        if (this.mailList.length) {
          this.selectedMessageChange(this.mailList[0]);
          this.selectedMessage = this.mailList[0];
        }
      }
    });
  }

  selectedMessageChange(selectedMessage) {
    console.log(selectedMessage, 'selectedMessage');
    this.selectedMessage = selectedMessage;
  }
}
