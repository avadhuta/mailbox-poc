import { Constants } from './../constants/constant';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../service/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  get loginFormControl() {
    return this.loginForm.controls;
  }

  login() {
    if (this.loginForm.valid) {
      const email = this.loginForm.controls['email'].value;
      const password = this.loginForm.controls['password'].value;
      this.userService.login(email, password).subscribe(
        (user) => {
          console.log(user, 'user');
          this.snackBar.open('Sucessfully Login', null, { duration: 2000 });
          this.router.navigate(['/dashboard']);
        },
        (err) => {
          console.log(err.status, 'err');
          if (err.status === 401) {
            this.router.navigate(['/login']);
          }
          alert('Username or password does not match, please try again');
        }
      );
    }
  }
}
