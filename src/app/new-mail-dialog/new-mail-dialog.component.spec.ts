import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewMailDialogComponent } from './new-mail-dialog.component';

describe('NewMailDialogComponent', () => {
  let component: NewMailDialogComponent;
  let fixture: ComponentFixture<NewMailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewMailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewMailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
