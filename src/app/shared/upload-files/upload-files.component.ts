import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { UploadFileService } from 'src/app/service/upload-file.service';
import { dataURItoBlob, getExactFilePath } from 'src/app/utilities/helper';
import { FileUploadService } from 'src/app/service/file-upload-service';

@Component({
  selector: 'app-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss'],
})
export class UploadFilesComponent implements OnInit {
  selectedFiles: FileList;
  currentFile: File;
  progress = 0;
  fileName = '';
  message = '';
  errMessage = '';
  fileInfos: Observable<any>;
  @Input() fileUrl = '';
  @Input() fileList = [];
  @Output() uploadedUrl = new EventEmitter();
  @Output() removeUploadedFile = new EventEmitter();

  constructor(private fileUploadService: FileUploadService) {}

  ngOnInit(): void {
    console.log(this.fileUrl, 'fileUrl');
  }

  selectFile(event): void {
    this.selectedFiles = event.target.files;
  }

  fileUpload(event) {
    const fileName = event.srcElement.value;
    let fileExt;
    if (fileName) {
      this.fileName = getExactFilePath(fileName);
      fileExt = this.fileName.split('.')[1].toLocaleLowerCase();
    }
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (data: any) => {
        // called once readAsDataURL is completed
        const dataUrl = data.currentTarget.result;
        const blobData = dataURItoBlob(dataUrl.split(',')[1], fileExt);
        console.log(blobData, 'blobData');
        this.fileUploadService.getUploadUrl(blobData.type).subscribe((url) => {
          if (url) {
            console.log(url, 'url');
            this.fileUploadService
              .uploadFileToS3(url.mapedurl, blobData, fileExt)
              .subscribe(
                (res) => {
                  this.fileUrl = url.mapedurl.split('?')[0];
                  this.uploadedUrl.emit(this.fileUrl);
                  console.log(res, 'res');
                  alert('File uploaded successfully');
                },
                (err) => {
                  console.log(err, 'err');
                  alert('Oops! something went wrong please try again later');
                }
              );
          }
          console.log(url, 'url');
        });
      };
    }
    console.log(fileName, fileExt, 'file');
  }

  removeFile() {
    this.fileUrl = '';
    this.removeUploadedFile.emit();
  }
}
