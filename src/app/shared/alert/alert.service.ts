import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Alert, AlertType } from './alert-model';

@Injectable({
    providedIn: 'root'
})
export class AlertService {
    private readonly isAlertSrc = new Subject<Alert>();

    getAlert(): Observable<any> {
        return this.isAlertSrc.asObservable();
    }

    alert(type, message) {
        this.isAlertSrc.next({ type: type, message: message });
    }

    success(message: string) {
        this.alert(AlertType.Success, message);
    }

    error(message: string) {
        this.alert(AlertType.Error, message);
    }

    info(message: string) {
        this.alert(AlertType.Info, message);
    }

    warn(message: string) {
        this.alert(AlertType.Warning, message);
    }

    message(message: string) {
        this.alert(AlertType.Message, message);
    }


    clear() {
        // clear alerts
        this.isAlertSrc.next();
    }

}
