export class Alert {
    type?: AlertType;
    message?: string;
    cssClass?: string;
}

export enum AlertType {
    Success,
    Error,
    Info,
    Warning,
    Message
}


export class Message {
    cssClass: string;
    message: string;
}

