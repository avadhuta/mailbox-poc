import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Alert, AlertType, Message } from './alert-model';
import { AlertService } from './alert.service';

@Component({
    selector: 'Jam-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
    alert: Alert;
    showAlert = false;
    constructor(private alertService: AlertService, private cdr: ChangeDetectorRef) { }

    ngOnInit() {
        this.alertService.getAlert().subscribe((alert: Alert) => {
            this.alert = alert;
            this.showAlert = true;
            this.cdr.detectChanges();
            setTimeout(() => {
                this.removeAlert();
            }, 3000);
        });
    }

    removeAlert() {
        this.showAlert = false;
        this.cdr.detectChanges();
    }

    getAlertType(alert: Alert): { cssClass: string, message: string } {
        if (!alert) {
            return undefined;
        }
        // return css class based on alert type
        switch (alert.type) {
            case AlertType.Success:
                return { cssClass: 'fa fa-check-circle', message: 'success' };
            case AlertType.Error:
                return { cssClass: 'fa fa-times', message: 'error' };
            case AlertType.Info:
                return { cssClass: 'fa fa-info', message: 'info' };
            case AlertType.Warning:
                return { cssClass: 'fa fa-warning', message: 'warning' };
            case AlertType.Message:
                return { cssClass: 'fa fa-envelope-o', message: 'message' };
        }
    }
}
