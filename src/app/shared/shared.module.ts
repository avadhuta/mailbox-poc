import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafeHtMlPipe } from './pipe/safehtml.pipe';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UploadFilesComponent } from './upload-files/upload-files.component';
import { AppMaterialModule } from '../app-material/app-material.module';

@NgModule({
  declarations: [SafeHtMlPipe, UploadFilesComponent],
  imports: [CommonModule, RouterModule, FormsModule, ReactiveFormsModule, AppMaterialModule],
  exports: [SafeHtMlPipe, UploadFilesComponent],
  entryComponents: [],
})
export class SharedModule {}
