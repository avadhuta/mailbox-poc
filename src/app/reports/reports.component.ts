import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { tap } from 'rxjs/operators';
import { NewsItem } from '../entities/newsitem';
import { SortField } from '../entities/sort-field';
import { NewsletterService } from '../service/newsletter.service';
import { ReportService } from '../service/report.service';
import { ReportDataSource } from '../utilities/report-data-source';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit {
  matcher;
  minDate: Date;
  maxDate: Date;
  endDate: Date;
  startDate: Date;
  selectedCat: string;
  dataSource: ReportDataSource;
  limit: number;
  page: number;
  newsItems: NewsItem[];
  sortField: SortField = {
    name: 'received_date',
    sortOrder: '-1',
  };
  total: number;
  displayedColumns: string[] = [
    'activity_date',
    "user",
    'entity',
    'field_name',
    'old_value',
    'new_value',
    'operation_type',
  ];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  classifications: any = [];
  showTable = false;

  constructor(
    private reportService: ReportService,
    private newsletterService: NewsletterService
  ) {
    this.limit = 50;
    this.page = 0;
  }

  ngOnInit(): void {
    this.dataSource = new ReportDataSource(this.reportService);
    this.getClassifications();
  }

  ngAfterViewInit() {
    this.paginator.page.pipe(tap(() => this.loadNewsitemsPage())).subscribe();
  }

  getAuditReport() {
    this.reportService.getAuditTrailReport(this.startDate, this.endDate, this.limit, this.page).subscribe(items => {
      console.log("ReportsComponent -> getAuditTrailReport -> res", items);
      if (items) {
        this.showTable = true;
        this.newsItems = items.data;
        this.total = items.total;
      }
    });
    this.loadNewsitemsPage();
  }

  getClassificationString(newsItem: any) {
    let classification = '';
    newsItem.classification &&
      newsItem.classification.map((item) => {
        if (item == 1) classification = classification + 'IA, ';
        if (item == 2) classification = classification + 'CQ, ';
        if (item == 3) classification = classification + 'NL, ';
      });
    classification = classification.substr(0, classification.length - 2);
    return classification;
  }

  loadNewsitemsPage() {
    this.dataSource.loadNewsitems(this.paginator.pageIndex, this.paginator.pageSize, this.startDate, this.endDate);
  }

  onPageChange(event) {
    console.log('UnclassifiedComponent -> onPageChange -> event', event);
    this.limit = event.pageSize;
    this.page = event.pageIndex;
    const startDate = new Date(this.startDate).getTime();
    const endDate = new Date(this.endDate).getTime();
    console.log(startDate, endDate, this.selectedCat, 'data');
    this.getAuditReport();
  }

  handleFilter() {
    const startDate = new Date(this.startDate).getTime();
    const endDate = new Date(this.endDate).getTime();
    if (startDate && endDate) {
      this.getAuditReport();
    } else {
      alert('Please select a start date and an end date');
    }
  }

  getClassifications() {
    this.newsletterService
      .getNewsItemClassifications()
      .subscribe((classes) => (this.classifications = classes));
  }

}
