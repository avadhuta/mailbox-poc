import { ConfigurationComponent } from './configuration/configuration.component';
import { ReportsComponent } from './reports/reports.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InboxComponent } from './inbox/inbox.component';
import { DashboardComponent } from './overview/dashboard.component';
import { MyTasksComponent } from './my-tasks/my-tasks.component';
import { NewslettersComponent } from './newsletters/newsletters.component';
import { SettingsComponent } from './settings/settings.component';
import { UsersComponent } from './users/users.component';
import { AllNewsItemsComponent } from './newsletters/all-news-items/all-news-items.component';
import { NewsItemInDetailComponent } from './newsletters/news-item-in-detail/news-item-in-detail.component';
import { RelevantFormComponent } from './newsletters/relevant-form/relevant-form.component';
import { IntiateAssessmentFormComponent } from './newsletters/intiate-assessment-form/intiate-assessment-form.component';
import { AssessmentDetailsComponent } from './newsletters/assessment-details/assessment-details.component';
import { ActionsComponent } from './newsletters/actions/actions.component';
import { AuthGuard } from './core/guard/auth.guard';
import { NewsitemsFromGraphComponent } from './newsletters/newsitems-from-graph/newsitems-from-graph.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'newsitems',
    component: AllNewsItemsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'newsitems/:id',
    component: NewsItemInDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'newsitems/status/change',
    component: RelevantFormComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'newsitems/graph/view',
    component: NewsitemsFromGraphComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'intiate-assessment',
    component: IntiateAssessmentFormComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'assessment-details',
    component: AssessmentDetailsComponent,
    canActivate: [AuthGuard],
  },
  { path: 'actions', component: ActionsComponent, canActivate: [AuthGuard] },
  { path: 'my-tasks', component: MyTasksComponent, canActivate: [AuthGuard] },
  {
    path: 'newsletters',
    component: NewslettersComponent,
    canActivate: [AuthGuard],
  },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },
  {
    path: 'configuration',
    component: ConfigurationComponent,
    canActivate: [AuthGuard],
  },

  { path: 'reports', component: ReportsComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'inbox', component: InboxComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
