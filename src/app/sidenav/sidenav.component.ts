import { UserService } from './../service/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserProfile } from '../entities/user';
import { User } from '../model/user';
import { MatDialog } from '@angular/material/dialog';
import { NewMailDialogComponent } from '../new-mail-dialog/new-mail-dialog.component';
import { MessageService } from '../service/message.service';

export interface Section {
  name: string;
  icon: string;
  path: string;
}

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {
  selectedUserEmail: string;
  users: UserProfile[];
  currentUser: User;
  selectedIndex = 0;
  selectedLabelIndex = null;
  isExpanded = false;

  folders: Section[] = [
    {
      name: 'Overview',
      icon: '../../assets/icons/dashboard.png',
      path: 'dashboard',
    },
    {
      name: 'Inbox',
      icon: '../../assets/icons/inbox.png',
      path: 'inbox',
    },
    {
      name: 'Newsletters',
      icon: '../../assets/icons/newsLetters.png',
      path: 'newsletters',
    },
    {
      name: 'All News Items',
      icon: '../../assets/icons/file.png',
      path: 'newsitems',
    },
    {
      name: 'Users',
      icon: '../../assets/icons/users.png',
      path: 'users',
    },
    {
      name: 'My Tasks',
      icon: '../../assets/icons/myTasks.png',
      path: 'my-tasks',
    },
    {
      name: 'Reports',
      icon: '../../assets/icons/report.png',
      path: 'reports',
    },
    {
      name: 'Configuration',
      icon: '../../assets/icons/document.png',
      path: 'configuration',
    },
    {
      name: 'Settings',
      icon: '../../assets/icons/settings.png',
      path: 'settings',
    },
  ];

  constructor(
    private router: Router,
    private messageService: MessageService,
    private userService: UserService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getCurrentUser();
    // this.getActiveMailBox();
    // setInterval(() => {
    //   this.messageService.onPushToMessageStack();
    // }, 1000);
  }

  getCurrentUser() {
    this.userService.getCurrentUserData().subscribe((user: User) => {
      if (user) {
        this.currentUser = user;
        console.log('current user', this.currentUser);
      }
    });
  }

  getActiveMailBox(): void {
    // this.userService.getUsers()
    //   .subscribe(users => {
    //     this.users = users;
    //     this.selectedUserEmail = this.users[0].email;
    //     this.userService.setMailBox(this.selectedUserEmail);
    //   });
  }

  onSetMailBox(event) {
    //this.userService.setMailBox(event.value);
  }

  onNavigateTo(path, index) {
    this.selectedIndex = index;
    if (path === 'newsitems') {
      this.router.navigate(['/' + path], { queryParams: { newsletter: null } });
    } else {
      this.router.navigate(['/' + path]);
    }
  }

  logOut() {
    this.router.navigate(['/login']);
    sessionStorage.clear();
  }

  openNewMailDialog() {
    const dialogRef = this.dialog.open(NewMailDialogComponent, {
      height: '400px',
      width: '600px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
