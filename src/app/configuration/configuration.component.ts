import { ReportService } from './../service/report.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss'],
})
export class ConfigurationComponent implements OnInit {
  configuration;
  constructor(private reportService: ReportService) {}

  ngOnInit(): void {
    this.getConfigList();
  }

  getConfigList() {
    this.reportService.getAllConfiguration().subscribe((configs) => {
      console.log(configs, 'configs');
      if (configs) {
        this.configuration = configs;
      }
    });
  }
}
