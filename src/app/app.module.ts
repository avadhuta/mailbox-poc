import { AppMaterialModule } from './app-material/app-material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InboxComponent } from './inbox/inbox.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { SidenavComponent } from './sidenav/sidenav.component';
import { MailboxContentComponent } from './mailbox-content/mailbox-content.component';
import { MailboxListComponent } from './mailbox-list/mailbox-list.component';
import { ReplyMailDialogComponent } from './reply-mail-dialog/reply-mail-dialog.component';
import { ForwardMailDialogComponent } from './forward-mail-dialog/forward-mail-dialog.component';
import { NewMailDialogComponent } from './new-mail-dialog/new-mail-dialog.component';
import { SearchFilterPipe } from './pipe/search.pipe';
import { DashboardComponent } from './overview/dashboard.component';
import { NewslettersComponent } from './newsletters/newsletters.component';
import { UsersComponent } from './users/users.component';
import { MyTasksComponent } from './my-tasks/my-tasks.component';
import { SettingsComponent } from './settings/settings.component';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UnclassifiedComponent } from './newsletters/unclassified/unclassified.component';
import { RelevantComponent } from './newsletters/relevant/relevant.component';
import { NotRelevantComponent } from './newsletters/not-relevant/not-relevant.component';
import { NewsItemInDetailComponent } from './newsletters/news-item-in-detail/news-item-in-detail.component';
import { AllNewsItemsComponent } from './newsletters/all-news-items/all-news-items.component';
import { httpInterceptorProviders } from './core/interceptor';
import { AuthTokenService } from './service/auth-token.service';
import { AuthGuard } from './core/guard/auth.guard';
import { LocalStorageModule } from 'angular-2-local-storage';
import { SharedModule } from './shared/shared.module';
import { ConfirmDialogComponent } from './utilities/confirm-dialog/confirm-dialog.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { RelevantFormComponent } from './newsletters/relevant-form/relevant-form.component';
import { IntiateAssessmentFormComponent } from './newsletters/intiate-assessment-form/intiate-assessment-form.component';
import { AssessmentDetailsComponent } from './newsletters/assessment-details/assessment-details.component';
import { ActionsComponent } from './newsletters/actions/actions.component';
import { AssessmentDialogComponent } from './newsletters/assessment-dialog/assessment-dialog.component';
import { AddActionDialogComponent } from './newsletters/add-action-dialog/add-action-dialog.component';
import { MatTableModule } from '@angular/material/table';
import { NewsitemsFromGraphComponent } from './newsletters/newsitems-from-graph/newsitems-from-graph.component';
import { ReportsComponent } from './reports/reports.component';
import { ConfigurationComponent } from './configuration/configuration.component';

@NgModule({
  declarations: [
    AppComponent,
    InboxComponent,
    LoginComponent,
    PageNotFoundComponent,
    SidenavComponent,
    MailboxContentComponent,
    MailboxListComponent,
    ReplyMailDialogComponent,
    ForwardMailDialogComponent,
    NewMailDialogComponent,
    SearchFilterPipe,
    DashboardComponent,
    NewslettersComponent,
    AllNewsItemsComponent,
    UsersComponent,
    MyTasksComponent,
    SettingsComponent,
    UnclassifiedComponent,
    RelevantComponent,
    NotRelevantComponent,
    NewsItemInDetailComponent,
    ConfirmDialogComponent,
    RelevantFormComponent,
    IntiateAssessmentFormComponent,
    AssessmentDetailsComponent,
    ActionsComponent,
    AssessmentDialogComponent,
    AddActionDialogComponent,
    NewsitemsFromGraphComponent,
    ReportsComponent,
    ConfigurationComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FlexLayoutModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    FormsModule,
    SharedModule,
    MatTableModule,
    ReactiveFormsModule,
    HttpClientModule,
    LocalStorageModule.forRoot({
      prefix: 'mail-box',
      storageType: 'localStorage',
    }),
    NgxChartsModule,
  ],
  entryComponents: [],
  providers: [httpInterceptorProviders, AuthTokenService, AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
