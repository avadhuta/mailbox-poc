# DEV SERVER DEPLOYMENT

1. On the local system, build the frontend project with `ng build --prod`
2. Copy the dist files from `{ProjectFolder}/dist/{ProjectName}/*.*` to the folder `rinext-ui/` on server. (Use SCP command)
3. cd to `rinext-ui/{ProjectName}/` 
4. Install lite-server (https://github.com/johnpapa/lite-server)
5. Run `nohup lite-server &`